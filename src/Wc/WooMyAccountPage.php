<?php


namespace GfWpPluginContainer\Wc;


class WooMyAccountPage
{

    public function init()
    {
        remove_action('woocommerce_account_navigation', 'woocommerce_account_navigation');

        add_filter('woocommerce_save_account_details_required_fields', [$this, 'removeFieldsEditAccount']);

        add_filter('woocommerce_billing_fields', [$this, 'removeFieldsAddress']);

        add_filter('woocommerce_billing_fields', [$this, 'removeFieldsEditAccount'], 20, 1);

        add_filter("woocommerce_billing_fields", [$this, 'orderBillingFields'], 20, 2);

        add_filter('woocommerce_account_orders_columns', [$this, 'orderTableFields'], 10, 1);

        add_filter('woocommerce_my_account_my_orders_query', [$this, 'customOrderQuery'], 20, 1);


    }

    public function orderBillingFields($fields)
    {
        $fields['billing_address_1']['priority'] = 10;
        $fields['billing_address_1']['placeholder'] = '';
        $fields['billing_city']['priority'] = 20;
        $fields['billing_postcode']['priority'] = 30;
        $fields['billing_country']['priority'] = 90;
        $fields['billing_phone']['priority'] = 100;

        return $fields;
    }

    //Removes required fields from edit account on my account page

    public function removeFieldsEditAccount($required_fields)
    {
        unset($required_fields['account_display_name']);
        return $required_fields;
    }

    public function removeFieldsAddress($fields)
    {
        unset($fields ['billing_state'], $fields ['billing_company'], $fields['billing_address_2']);
        return $fields;
    }


    public function orderTableFields($fields)
    {
        $fields = [
            'order-number' => 'Order',
            'order-date' => 'Date',
            'order-total' => 'Total',
            'order-status' => 'Status',
        ];

        return $fields;
    }

    //removes pagination from orders
    public function customOrderQuery($args)
    {
        $args['limit'] = -1;

        return $args;
    }

}