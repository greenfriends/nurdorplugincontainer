<?php


namespace GfWpPluginContainer\Wc;


class WooOrderAdmin
{
    public function init()
    {
        add_filter('manage_edit-shop_order_columns', [$this, 'newOrderColumn']);
        add_action('manage_shop_order_posts_custom_column', [$this, 'newOrderColumnPopulate']);
    }

    public function newOrderColumn($columns)
    {
        $newColumns = [];
        foreach ($columns as $colName => $colData) {
            $newColumns[$colName] = $colData;

            if ($colName === 'order_status') {
                $newColumns['order_type'] = __('Tip narudžbine', 'gfShopTheme');
                $newColumns['payment_type'] = __('Tip plaćanja', 'gfShopTheme');
            }
        }
        return $newColumns;
    }

    function newOrderColumnPopulate($column)
    {
        global $post;
        if ('order_type' === $column) {
            echo ucfirst(wc_get_order($post->ID)->get_meta('gfOrderType'));
        }
        if ('payment_type' === $column) {
            echo ucfirst(wc_get_order($post->ID)->get_meta('gfPaymentType'));
        }
    }
}