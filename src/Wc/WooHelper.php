<?php

namespace GfWpPluginContainer\Wc;

use \TNP;

class WooHelper
{
    /**
     * All actions and filters
     */
    public function init()
    {
        add_filter('woocommerce_add_to_cart_fragments', [$this, 'updateCart'], 10, 1);
        add_action('woocommerce_register_post', [$this, 'validateExtraRegistrationData'], 10, 3);
        add_action('woocommerce_created_customer', [$this, 'saveExtraRegistrationData']);
        add_filter('woocommerce_registration_redirect', [$this, 'redirectAfterRegister']);
        add_filter('woocommerce_currency_symbol', [$this, 'changeWooCurrencySymbol'], 10, 2);
        add_action('template_redirect', [$this, 'bypassLogoutConfirmation']);
        add_action('woocommerce_after_checkout_validation', [$this, 'checkoutValidation'], 10, 2);
        add_filter('woocommerce_checkout_fields', [$this, 'removeDefaultCheckoutValidation'],10, 2);
    }

    /**
     * Function is used to update cart totals on frontend, not sure how it works :D
     * @param $fragments
     * @return mixed
     */
    public function updateCart($fragments)
    {

        $fragments['div.cart-count'] = '<div class="cart-count">
                                           <span>
                                             ' . WC()->cart->get_cart_contents_count() . '
                                            </span>
                                        </div>';

        return $fragments;

    }

    /**
     * Handling validation of custom fields added to woocommerce
     * @param $username
     * @param $email
     * @param $validation_errors
     * @todo implement validation for length and prevention of special characters
     */
    public function validateExtraRegistrationData($username, $email, $validation_errors)
    {
        if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
            $validation_errors->add('billing_first_name_error', __('Ime je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
            $validation_errors->add('billing_last_name_error', __('Prezime je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_address_1']) && empty($_POST['billing_address_1'])) {
            $validation_errors->add('billing_address_1_error', __('Adresa je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_city']) && empty($_POST['billing_city'])) {
            $validation_errors->add('billing_city_error', __('Grad je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_postcode']) && empty($_POST['billing_postcode'])) {
            $validation_errors->add('billing_postcode_error', __('Poštanski broj je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_phone']) && empty($_POST['billing_phone'])) {
            $validation_errors->add('billing_phone_error', __('Kontakt telefon je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_address_1']) && empty($_POST['billing_address_1'])) {
            $validation_errors->add('billing_address_1_error', __('Došlo je do neočekivane greške!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_street']) && empty($_POST['billing_street'])) {
            $validation_errors->add('billing_street_error', __('Ulica je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_street_number']) && empty($_POST['billing_street_number'])) {
            $validation_errors->add('billing_street_number_error', __('Broj ulice je obavezno polje!', 'gfShopTheme'));
        }
        if (!isset($_POST['billing_municipality']) || $_POST['billing_municipality'] === '-1') {
            $validation_errors->add('billing_municipality_error', __('Opština je obavezno polje!', 'gfShopTheme'));
        }
        if(isset($_POST['billing_apartment_number']) && strlen($_POST['billing_apartment_number']) > 8) {
            $validation_errors->add('billing_apartment_number_error', __('Broj Stana ne može imati više od 8 karaktera!', 'gfShopTheme'));
        }
    }

    /**
     * Updates user meta with custom fields woocommerce uses for customer user type
     * @param $customerId
     */
    public function saveExtraRegistrationData($customerId)
    {
        if (isset($_POST['billing_first_name'])) {
            update_user_meta($customerId, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
        }
        if (isset($_POST['billing_last_name'])) {
            update_user_meta($customerId, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
        }
        if (isset($_POST['email'])) {
            update_user_meta($customerId, 'billing_email', sanitize_text_field($_POST['email']));
        }
        if (isset($_POST['billing_address_1'])) {
            update_user_meta($customerId, 'billing_address_1', sanitize_text_field($_POST['billing_address_1']));
        }
        if (isset($_POST['billing_city'])) {
            update_user_meta($customerId, 'billing_city', sanitize_text_field($_POST['billing_city']));
        }
        if (isset($_POST['billing_postcode'])) {
            update_user_meta($customerId, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));
        }
        if (isset($_POST['billing_phone'])) {
            update_user_meta($customerId, 'billing_phone', sanitize_text_field($_POST['billing_phone']));
        }
        if (isset($_POST['billing_country'])) {
            update_user_meta($customerId, 'billing_country', sanitize_text_field($_POST['billing_country']));
        }
        if (isset($_POST['billing_street'])) {
            update_user_meta($customerId, 'billing_street', sanitize_text_field($_POST['billing_street']));
        }
        if (isset($_POST['billing_street_number'])) {
            update_user_meta($customerId, 'billing_street_number', sanitize_text_field($_POST['billing_street_number']));
        }
        if (isset($_POST['billing_apartment_number'])) {
            update_user_meta($customerId, 'billing_apartment_number', sanitize_text_field($_POST['billing_apartment_number']));
        }
        if (isset($_POST['billing_municipality'])) {
            update_user_meta($customerId, 'billing_municipality', sanitize_text_field($_POST['billing_municipality']));
        }

        update_user_meta($customerId, 'newUser', true);
        if (($_POST['newsletter'] === '1') && class_exists(TNP::class)) {
            TNP::subscribe(['email' => $_POST['email'], 'status' => 'C']);
        }
    }

    /**
     * Redirects user to my account dashboard after registration
     * @param $redirect
     * @return string
     */
    public function redirectAfterRegister($redirect)
    {

        return wc_get_page_permalink('myaccount');
    }

    /**
     * Changes currency symbol from din to rsd
     * @param $currency_symbol
     * @param $currency
     * @return string
     */
    public function changeWooCurrencySymbol($currency_symbol, $currency)
    {
        if ($currency == 'RSD') {
            $currency_symbol = 'RSD';
        }
        return $currency_symbol;
    }

    /**
     * Bypass logout confirmation.
     */
    public function bypassLogoutConfirmation()
    {
        global $wp;

        if (isset($wp->query_vars['customer-logout'])) {
            wp_redirect(str_replace('&amp;', '&', wp_logout_url(get_home_url())));
            exit;
        }
    }

    public function removeDefaultCheckoutValidation($fields){
        foreach ($fields as $field) {
            $field['required'] = false;
        }
        return $fields;
    }

    public function checkoutValidation($fields, $errors)
    {
        if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name'])) {
            $errors->add('validation', __('<strong>Ime</strong> je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name'])) {
            $errors->add('validation', __('<strong>Prezime</strong> je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_address_1']) && empty($_POST['billing_address_1'])) {
            $errors->add('validation', __('<strong>Adresa</strong> je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_city']) && empty($_POST['billing_city'])) {
            $errors->add('validation', __('<strong>Grad</strong> je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_postcode']) && empty($_POST['billing_postcode'])) {
            $errors->add('validation', __('<strong>Poštanski broj</strong> je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_phone']) && empty($_POST['billing_phone'])) {
            $errors->add('validation', __('<strong>Kontakt</strong> telefon je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_address_1']) && empty($_POST['billing_address_1'])) {
            $errors->add('validation', __('<strong>Došlo je do neočekivane greške!</strong>', 'gfShopTheme'));
        }
        if (isset($_POST['billing_street']) && empty($_POST['billing_street'])) {
            $errors->add('validation', __('<strong>Ulica</strong> je obavezno polje!', 'gfShopTheme'));
        }
        if (isset($_POST['billing_street_number']) && empty($_POST['billing_street_number'])) {
            $errors->add('validation', __('<strong>Broj ulice</strong>  je obavezno polje!', 'gfShopTheme'));
        }
        if (!isset($_POST['billing_municipality']) || $_POST['billing_municipality'] === '-1') {
            $errors->add('validation', __('<strong>Opština</strong> je obavezno polje!', 'gfShopTheme'));
        }
    }

}