<?php

namespace GfWpPluginContainer\Wc;


use GF\Corvus\Token\Service\Ajax;

class MasterCardOnlyHandler
{
    public function init()
    {
        $this->checkIfUserCanBuyMasterCardOnlyProduct();
        $this->addMasterCardOnlyNoticeToProductPage();
        $this->preventCheckoutIfNoMastercard();
    }

    private function checkIfUserCanBuyMasterCardOnlyProduct()
    {
        add_action('woocommerce_add_to_cart_validation', function ()
        {
            $cart = WC()->cart;
            $product = wc_get_product($_POST['add-to-cart']);
            $masterCardOnlyProduct = false;
            if ($product && $product->get_meta('masterCard', true) === 'yes' && !empty(WC()->cart->get_cart())) {
                $masterCardOnlyProduct = true;
            }
            foreach ($cart->get_cart() as $cartItem) {
                $product = $cartItem['data'];
                if ($product instanceof \WC_Product_Variation) {
                    $product = wc_get_product($product->get_parent_id());
                }
                if (($masterCardOnlyProduct === true)) {
                    if ($product->get_meta('masterCard', true) !== 'yes') {
                        $cart->remove_cart_item($cartItem['key']);
                    }
                } elseif ($product->get_meta('masterCard', true) === 'yes') {
                    $msg = __('U korpi postoji poklon koji se dobija samo uz donaciju Mastercard karticom i ne može se kombinovati sa ostalim poklonima.',
                        'gfCorvusPayment');
                    $_POST['mastercardNotice'] = $msg;
                    return false;
                }
            }
            return true;
        }, 10);
    }

    private function addMasterCardOnlyNoticeToProductPage()
    {
        add_action('woocommerce_after_add_to_cart_quantity', function ()
        {
            global $product;
            $subtitle = get_post_meta($product->get_id(),'post_subtitle',true);
            if ($product->get_meta('masterCard', true) === 'yes') {
                echo '<p class="masterCardOnlySingleNotice">' . __('Ovaj poklon se dobija samo uz donaciju Mastercard karticom i ne može se kombinovati sa ostalim poklonima.',
                        'gfShopTheme') . '</p>';
            }
        });
    }

    /**
     * Checks if user has mastercard as default payment method when trying to pay with saved card when mastercard only product is in cart
     */
    private function preventCheckoutIfNoMastercard()
    {
        add_action('woocommerce_after_checkout_validation', function ($fields, $errors)
        {
            if (isset($_POST['payWithSavedCard']) && $_POST['payWithSavedCard'] === '1') {
                $masterCardOnlyProduct = false;
                foreach (WC()->cart->get_cart() as $cartItem) {
                    $product = $cartItem['data'];
                    if ($product instanceof \WC_Product_Variation) {
                        $product = wc_get_product($product->get_parent_id());
                    }
                    if ($product->get_meta('masterCard', true) === 'yes') {
                        $masterCardOnlyProduct = true;
                        break;
                    }
                }
                if ($masterCardOnlyProduct === true) {
                    $tokenService = new Ajax();
                    if (!$tokenService->isMasterCardUser(get_current_user_id(), false)) {
                        $errors->add('validation',
                            __('U korpi postoji poklon koji se dobija samo uz donaciju Mastercard karticom, izaberite Mastercard karticu kao podrazumevan način plaćanja i pokušajte opet.',
                                'gfShopTheme'));
                    }
                }
            }
        }, 10, 2);
    }
}