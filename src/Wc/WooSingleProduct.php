<?php


namespace GfWpPluginContainer\Wc;


class WooSingleProduct
{
    public function init()
    {
        $this->removeActions();
    }

    public function removeActions()
    {
        remove_action('woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',10);
        remove_action('woocommerce_after_single_product_summary','woocommerce_upsell_display', 15);
        remove_action('woocommerce_after_single_product_summary','woocommerce_output_related_products', 20);
        remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
        remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
        remove_action('woocommerce_before_single_product','woocommerce_output_all_notices' ,10);
        add_action('woocommerce_before_single_product','wc_clear_notices', 10);
        remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);
        add_action('woocommerce_after_single_product_summary', [$this, 'displayFeaturedProducts'], 11);
    }

    public function displayFeaturedProducts (){
        echo do_shortcode('[gf_featured_products]');
    }
}