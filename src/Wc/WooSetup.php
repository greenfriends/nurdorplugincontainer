<?php


namespace GfWpPluginContainer\Wc;

require_once(__DIR__ . '/WooMyAccountPage.php');
require_once(__DIR__ . '/WooHelper.php');
require_once(__DIR__ . '/WooSingleProduct.php');
require_once(__DIR__ . '/WooProductAdmin.php');
require_once(__DIR__ . '/WooOrderAdmin.php');
require_once(__DIR__ . '/WooOrder.php');

class WooSetup
{
    public function initClasses()
    {
        $wooMyAccount = new WooMyAccountPage();
        $wooMyAccount->init();

        $wooHelper = new WooHelper();
        $wooHelper->init();

        $wooSingleProduct = new WooSingleProduct();
        $wooSingleProduct->init();

        $wooProductAdmin = new WooProductAdmin();
        $wooProductAdmin->init();

        $wooOrderAdmin = new WooOrderAdmin();
        $wooOrderAdmin->init();

        $wooOrder = new WooOrder();
        $wooOrder->init();
    }
}