<?php

namespace GfWpPluginContainer\BexCourierIntegration\Logger;

class Logger
{
    public static function dbLog($message, int $statusCode, int $orderId, $response = null, $request = null): void
    {
        if (is_array($request)) {
            $request = json_encode($request);
        }
        global $wpdb;
        $wpdb->insert('gf_bex_shipments_log', [
            'message' => $message,
            'statusCode' => $statusCode,
            'orderId' => $orderId,
            'response' => $response ?? 'N/A',
            'request' => $request ?? 'N/A',
        ]);
    }
}