<?php

namespace GfWpPluginContainer\BexCourierIntegration\Api;
use GfWpPluginContainer\BexCourierIntegration\Logger\Logger;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class RequestPostShipments
{
    private $url;
    /**
     * @var array
     */
    private $config;
    /**
     * These are params that will be fixed for every shipment, rest are added via setters.
     *
     * You must be logged in gmail account that has access to Green Friends drive folder
     *
     * @link https://docs.google.com/document/d/15FRh9sKkQEbM0XUqPW8PCEh4cyCvTtmv/edit?usp=sharing&ouid=106253243560600248402&rtpof=true&sd=true
     * @var array
     */
    private $params =
        [
            'shipmentId' => 0,
            'serviceSpeed' => 1,
            'shipmentType' => 1,
            'shipmentCategory' => 1,
            'shipmentWeight' => 0,
            'totalPackages' => 1,
            'invoiceAmount' => 0,
            'shipmentContents' => 11, //Category Odeća
            'commentPublic' => '',
            'commentPrivate' => '',
            'personalDelivery' => true,
            'returnSignedInvoices' => false,
            'returnSignedConfirmation' => false,
            'returnPackage' => false,
            'payType' => 6,
            'insuranceAmount' => 0,
            'payToSender' => 0,
            'payToSenderViaAccount' => false,
            'sendersAccountNumber' => '',
            'bankTransferComment' => '',
            'tasks' => [],
            'reports' => [
                [
                    'mode' => 1, //email
                    'type' => 3, // izveštaj o preuzimanju
                ],
                [
                    'mode' => 1, //email
                    'type' => 3, // izveštaj o dostavi
                ]
            ]
        ];
    /**
     * @var \WC_Order
     */
    private $order;

    public function __construct($order)
    {
        $this->config = require PLUGIN_DIR . 'src/BexCourierIntegration/config/config.php';
        $this->params['reports'][0]['adress'] = 'info@superhero.rs';
        $this->params['reports'][1]['adress'] = 'info@superhero.rs';
        $this->order = $order;
        $this->url = $this->config['apiUrl'] . '/ship/api/Ship/postShipments';
    }

    private function setupPickupTask(): void
    {
        $orderItems = $this->order->get_items();
        $productIds = '';
        foreach ($orderItems as $item){
            $product = wc_get_product($item->get_product_id());
            $variation = wc_get_product($item->get_variation_id());
            if ($product && $variation) {
                $productIds .= $product->get_id() . '-' .  $variation->get_attribute('Size') . ' x '.$item->get_quantity() .'; ';
            } elseif ($product) {
                $productIds .= $product->get_id() . ' x '.$item->get_quantity() .'; ';
            } elseif ($variation) {
                $productIds .= $variation->get_id() . ' x '.$item->get_quantity() .'; ';
            }
        }
        $this->params['commentPublic'] = $productIds;
        $this->params['tasks'][] = [
            'type' => 1, // preuzimanje
            'nameType' => 3,
            'name1' => '238372',
            'name2' => '',
            'taxId' => '',
            'adressType' => 1,
            'municipalites' => '40',
            'place' => '',
            'street' => '',
            'houseNumber' => '16',
            'apartment' => '',
            'contactPerson' => '',
            'phone' => '',
            'date' => '',
            'pickupTimeFrom' => '09:00',
            'pickupTimeTo' => '14:55', //14:55 is end time for courier pickups
            'emailForReports' => 'info@superhero.rs',
            'comment' => '',
            'parcelShop' => 0
        ];
    }
    
    public function setDeliveryTask(): void
    {
        $info = $this->parseHouseAndApartmentNumber();
        $this->params['tasks'][] = [
            'type' => 2, // dostava
            'nameType' => 1, // fizicko lice
            'name1' => $this->order->get_billing_last_name(),
            'name2' => $this->order->get_billing_first_name(),
            'taxId' => '',
            'adressType' => 1,
            'municipalites' => $this->order->get_meta('billing_municipality', true),
            'place' => $this->order->get_billing_city(),
            'street' => $this->order->get_meta('billing_street', true),
            'houseNumber' => $info['houseNumber'],
            'apartment' => $info['apartmentNumber'],
            'contactPerson' => "{$this->order->get_billing_last_name()} {$this->order->get_billing_first_name()}",
            'phone' => $this->order->get_billing_phone(),
            'date' => '',
            'preNotification' => 60,
            'comment' => ''
        ];
    }

    /**
     * @return array
     * Moves non-numeric values of billing street number to apartment number because api doesn't accept non-numeric
     * characters inside street number. And per their documentation apartment number should look like this.
     * case street number is '5-17' and apartment number is 5 then apartment number should be '-17/5' and street number 5
     * case street number is '5z' and apartment number is 5 then apartment number should be 'z/5' and street number 5
     */
    public function parseHouseAndApartmentNumber(): array
    {
        $billingStreetNumber = $this->order->get_meta('billing_street_number', true);
        $houseNumber = (int)$billingStreetNumber;
        $apartmentNumber = str_replace($houseNumber,'',$billingStreetNumber);
        if ($apartmentNumber !== '') {
            $apartmentNumber .='/' . $this->order->get_meta('billing_apartment_number', true);
        } else {
            $apartmentNumber =$this->order->get_meta('billing_apartment_number', true);
        }
        $apartmentNumber = str_replace('ulaz', '', $apartmentNumber);
        return [
            'houseNumber' => $houseNumber,
            'apartmentNumber' => mb_substr($apartmentNumber, 0 , 8) // max 8 characters
        ];
    }

    public function send(): void
    {
        try {
            $this->setupPickupTask();
            $this->setDeliveryTask();
            $client = new Client;
            $params = [
                'shipmentsList' => [
                    $this->params
                ],
                'lang' => 2
            ];
            $params = json_encode($params, JSON_UNESCAPED_UNICODE);
            $requestOptions = [
                'headers' => ['X-Auth-Token' => $this->config['token'], 'Content-Type' => 'application/json'],
                'body' => $params
            ];
            $response = $client->request('POST', $this->url, $requestOptions);
            Logger::dbLog('Post shipment api request: ' . $params, $response->getStatusCode(), $this->order->get_id(),
                $response->getBody()->getContents());
            if ($response->getStatusCode() === 200) {
                $parsedBody = json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);
                $shipmentResult = $parsedBody['shipmentsResultList'][0];
                if ($shipmentResult['state'] === true) {
                    $shipmentId = $shipmentResult['shipmentId'];
                    $this->order->update_meta_data('shipmentId', $shipmentId);
                    $this->order->set_status('zakazano-slanje',
                        'Zakazan dolzak kurira broj pošiljke je ' . $shipmentId);
                    $this->order->save();
                    return;
                }
                Logger::dbLog(sprintf('Error in creating shipment: %s', $shipmentResult['err']),
                    $response->getStatusCode(), $this->order->get_id(), $response->getBody(), $params);
                return;
            }
            Logger::dbLog('Error in creating shipment status code not 200',
                $response->getStatusCode(), $this->order->get_id(), $response->getBody());
        } catch (\Exception $e) {
            Logger::dbLog('Unknown error', $e->getCode(), $this->order->get_id(), $e->getMessage(), $params ?? 'N/A');
            wp_mail('milos.jovanov@greenfreinds.systems', 'Nurdor bex integracija GRESKA',
                'GRESKA: ' . $e->getMessage());
        } catch (GuzzleException $e) {
            Logger::dbLog('Unknown error', $e->getCode(), $this->order->get_id(), $e->getMessage(), $params ?? 'N/A');
            wp_mail('milos.jovanov@greenfreinds.systems', 'Nurdor bex integracija GRESKA',
                '<p>GRESKA: ' . $e->getMessage() . '</p>');
        }
    }
}