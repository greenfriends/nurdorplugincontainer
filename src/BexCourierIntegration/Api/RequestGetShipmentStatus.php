<?php

namespace GfWpPluginContainer\BexCourierIntegration\Api;

use GfWpPluginContainer\BexCourierIntegration\Logger\Logger;
use GuzzleHttp\Client;

class RequestGetShipmentStatus
{
    private $url;

    /**
     * @var \WC_Order
     */
    private $order;

    public function __construct(\WC_Order $order)
    {
        $config = require PLUGIN_DIR . 'src/BexCourierIntegration/config/config.php';
        $this->order = $order;
        $this->url = $config['apiUrl'] . '/ship/api/Ship/getstate';
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(): ?string
    {
        $client = new Client;
        $shipmentId = $this->order->get_meta('shipmentId', true);
        $url = $this->url . "?shipmentid=$shipmentId&mtype=2&lang=1";
        $response = $client->request('GET', $url,
            ['headers' => ['X-Auth-Token' => 'd18465-45run-1337-dor-10of39', 'Content-Type' => 'application/json']]);
        $responseResult = $response->getBody()->getContents();
        Logger::dbLog("Get shipment status api request: $url", $response->getStatusCode(), $this->order->get_id(),
            $responseResult);

        if ($response->getStatusCode() === 200) {
            return $responseResult;
        }
        Logger::dbLog('Error in getting status status code not 200', $response->getStatusCode(), $this->order->get_id(),
            $responseResult);
        return null;
    }
}