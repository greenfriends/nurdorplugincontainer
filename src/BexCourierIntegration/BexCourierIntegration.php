<?php

namespace GfWpPluginContainer\BexCourierIntegration;

use GfWpPluginContainer\BexCourierIntegration\Api\RequestGetShipmentStatus;
use GfWpPluginContainer\BexCourierIntegration\Api\RequestPostShipments;
use GfWpPluginContainer\BexCourierIntegration\Logger\Logger;
use GfWpPluginContainer\BexCourierIntegration\WcStatuses\WcStatuses;
use GuzzleHttp\Exception\GuzzleException;

class BexCourierIntegration
{
    public function init(): void
    {
        $courierIntegrationStatuses = new WcStatuses();
        $courierIntegrationStatuses->createStatusesForShipmentHandling();
        $this->addMunicipalityField();
        if (defined('ENVIRONMENT') && ENVIRONMENT === 'prod') {
            $this->postShipment();
            $this->scheduleShipmentStatusCheck();
            $this->scheduleCreateShipmentRequest();
            add_action('checkOrdersShipmentStatus', [$this, 'changeOrderStatusBasedOnShipmentStatus']);
            add_action('postShipments', [$this, 'cronPostShipments']);
            add_action('woocommerce_checkout_update_order_meta', [$this, 'saveShipmentRequiredFields']);
        }
        if (defined('WP_CLI') && WP_CLI) {
            \WP_CLI::add_command('checkOrdersShipmentStatus',[$this, 'changeOrderStatusBasedOnShipmentStatus']);
            \WP_CLI::add_command('postShipments',[$this, 'cronPostShipments']);
        }
    }

    private function postShipment(): void
    {
        add_action('gfPaymentComplete', static function (\WC_Order $order)
        {
            $requestShipmentApi = new RequestPostShipments($order);
            $requestShipmentApi->send();
        });
    }

    private function addMunicipalityField(): void
    {
        add_filter('woocommerce_default_address_fields', function ($fields)
        {
            if (!is_account_page()) {
                return $fields;
            }
            $options = [];
            foreach ($this->getMunicipalities() as $municipality) {
                $options[$municipality->id] = $municipality->name;
            }
            $fields['municipality'] = [
                'label' => __('Municipality', 'gfShopTheme'),
                'required' => false,
                'class' => [],
                'priority' => 30,
                'autocomplete' => false,
                'type' => 'select',
                'options' => $options
            ];
            return $fields;
        });
    }

    public function getMunicipalities()
    {
        global $wpdb;
        $result = $wpdb->get_results(
            "SELECT * FROM gf_bex_municipalities ORDER BY `name`;"
        );
        return $result;
    }

    private function scheduleShipmentStatusCheck(): void
    {
        if (!wp_next_scheduled('checkOrdersShipmentStatus')) {
            wp_schedule_event(strtotime('00:00:00'), 'daily', 'checkOrdersShipmentStatus');
        }
    }

    private function scheduleCreateShipmentRequest(): void
    {
        if (!wp_next_scheduled('postShipments')) {
            wp_schedule_event(strtotime('14:00:00'), 'daily', 'postShipments');
        }
    }

    public function changeOrderStatusBasedOnShipmentStatus(): void
    {
        $orders = wc_get_orders([
            'status' => ['zakazano-slanje', 'u-tranzitu'],
            'order' => 'ASC',
            'numberposts' => -1
        ]);
        foreach ($orders as $order) {
            $api = new RequestGetShipmentStatus($order);
            try {
                $status = $api->send();
                if ($status === '2-izbrisana') {
                    $order->set_status('cancelled', 'Pošiljka je izbrisana zbog neaktivnosti');
                    $order->save();
                }
                if (($status === '4-preuzeta') && $order->get_status() !== 'u-tranzitu') {
                    $order->set_status('u-tranzitu', 'Kurir je pokupio pošiljku');
                    $order->save();
                }
                if ($status === '5-isporucena') {
                    $order->set_status('completed', 'Kurir je dostavio pošiljku');
                    $order->save();
                }
                if ($order->get_date_modified() < new \DateTime('-30 day')) {
                    $order->set_status('completed', 'Prošlo 30 dana od kreiranja pošiljke, a api nije vratio status');
                    $order->save();
                }
//                var_dump('orderId:' . $order->get_id() . ' status:' . $status);
            } catch (GuzzleException $e) {
                Logger::dbLog('Unknown error', $e->getCode(), $order->get_id(), $e->getMessage());
                wp_mail('milos.jovanov@greenfreinds.systems', 'Nurdor bex integracija GRESKA',
                    '<p>GRESKA: ' . $e->getMessage() . '</p>');
            }
        }
    }

    public function saveShipmentRequiredFields($order_id): void
    {
        $order = wc_get_order($order_id);
        if (isset($_POST['billing_street'])) {
            $order->update_meta_data('billing_street', $_POST['billing_street']);
        }
        if (isset($_POST['billing_street_number'])) {
            $order->update_meta_data('billing_street_number', $_POST['billing_street_number']);
        }
        if (isset($_POST['billing_apartment_number'])) {
            $order->update_meta_data('billing_apartment_number', $_POST['billing_apartment_number']);
        }
        if (isset($_POST['billing_municipality'])) {
            $order->update_meta_data('billing_municipality', $_POST['billing_municipality']);
        }
        $order->save();
    }

    public function cronPostShipments(): void
    {
        $orders = wc_get_orders([
            'status' => ['placeno'],
            'numberposts' => -1
        ]);
        foreach ($orders as $order) {
            $requestPostShipments = new RequestPostShipments($order);
            $requestPostShipments->send();
        }
    }
}