<?php

namespace GfWpPluginContainer\BexCourierIntegration\WcStatuses;

class WcStatuses
{
    public function createStatusesForShipmentHandling()
    {
        add_action('init', [$this, 'registerStatuses'], 99,2);
        add_filter('wc_order_statuses', [$this, 'listStatuses'], 99,2);
    }

    public function registerStatuses()
    {
        register_post_status('wc-zakazano-slanje', [
            'label' => 'Kurir još nije preuzeo poklon iz magacina',
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Kurir još nije preuzeo poklon iz magacina (%s)', ' Kurir još nije preuzeo poklon iz magacina (%s)', 'woocommerce')
        ]);
        register_post_status('wc-u-tranzitu', [
            'label' => 'Kurir je preuzeo poklon i na putu je ka vama',
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Kurir je preuzeo poklon i na putu je ka vama (%s)', 'Kurir je preuzeo poklon i na putu je ka vama (%s)', 'woocommerce')
        ]);
        register_post_status('wc-vracena-posiljka', [
            'label' => 'Vraćena posiljka',
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Vraćena posiljka (%s)', 'Vraćena posiljka (%s)', 'woocommerce')
        ]);
    }

    public function listStatuses($orderStatuses)
    {
        // add new order status after processing
        foreach ($orderStatuses as $key => $status) {
            $newOrderStatuses[$key] = $status;
            if ('wc-placeno' === $key) {
                $newOrderStatuses['wc-zakazano-slanje'] = 'Kurir još nije preuzeo poklon iz magacina';
                $newOrderStatuses['wc-u-tranzitu'] = 'Kurir je preuzeo poklon i na putu je ka vama';
            }
            if ('wc-completed' === $key) {
                $newOrderStatuses['wc-vracena-posiljka'] = 'Vraćena posiljka';
            }
        }
        return $newOrderStatuses;
    }
}