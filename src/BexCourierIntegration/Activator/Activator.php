<?php

namespace GfWpPluginContainer\BexCourierIntegration\Activator;

use PhpOffice\PhpSpreadsheet\IOFactory;

class Activator
{

    /**
     * @var \wpdb
     */
    private $db;

    public function __construct()
    {
        global $wpdb;
        $this->db = $wpdb;
    }

    public function createLogTable(): void
    {
        $tableName = 'gf_bex_shipments_log';
        $charsetCollate = $this->db->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS $tableName (
          id int(5) unsigned NOT NULL AUTO_INCREMENT,
          message LONGTEXT NOT NULL,
          response LONGTEXT,
          request LONGTEXT,
          orderId int(5) unsigned NOT NULL,
          statusCode int(4) unsigned NOT NULL,
          createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY  (id)
        ) $charsetCollate;";
        $this->db->query($sql);
    }
    public function createMunicipalitiesTable(): void
    {
        $tableName = 'gf_bex_municipalities';
        $charsetCollate = $this->db->get_charset_collate();

        $sql = "CREATE TABLE IF NOT EXISTS $tableName (
          id int(3) NOT NULL,
          name varchar(30) NOT NULL,
          PRIMARY KEY  (id)
        ) $charsetCollate;";
        $this->db->query($sql);
    }

    public function populateMunicipalityTable()
    {
        $municipalitiesArray = $this->parseMunicipalitiesExcel();
        foreach ($municipalitiesArray as $municipality) {
            $this->db->insert('gf_bex_municipalities', ['id' => $municipality['municipalityId'], 'name' => $municipality['municipalityName']]);
        }
    }

    private function parseMunicipalitiesExcel()
    {
        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);
        $spreadsheet = $reader->load(__DIR__ . '/../Data/Municipalities.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();
        $array = [];
        foreach ($worksheet->getRowIterator() as $row) {
            if ($row->getRowIndex() === 1) {
                continue;
            }
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            $i = 0;
            $tmpArray = [];
            foreach ($cellIterator as $cell) {
                if ($cell->getValue() !== '') {
                    if ($i === 0) {
                        $tmpArray['municipalityId'] = $cell->getValue();
                    }
                    if ($i === 1) {
                        $tmpArray['municipalityName'] = $cell->getValue();
                    }
                }
                $i++;
            }
            $array[] = $tmpArray;
        }
        return $array;
    }
}