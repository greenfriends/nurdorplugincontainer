<?php

namespace GfWpPluginContainer\AutoTerms\Mailer;

use Laminas\Mail\Transport\Smtp;

class Mailer
{
    public function sendTermsUpdateEmail(string $to, string $subject, string $termPageUrl, string $heading, string $innerText): void
    {
        $headers = array('Content-Type: text/html; charset=UTF-8');
        ob_start();
        include __DIR__ .'/../templates/termsUpdateEmail.php';
        $bodyContent = ob_get_clean();
        $result = wp_mail( $to, $subject, $bodyContent, $headers );
        if (!$result) {
            $this->log(sprintf('Failed sending email to %s', $to));
        }
    }

    private function log($msg)
    {
        $file = WP_CONTENT_DIR . '/uploads/termsUpdateLog.txt';
        $msg = date('Y/m/d H:i:s') .' : '. $msg;
        $data = file_get_contents($file) . PHP_EOL . PHP_EOL . $msg;
        file_put_contents($file, $data);
    }
}