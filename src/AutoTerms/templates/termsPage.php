<?php

/**
 * @var WP_POST[] $posts
 * @see AutoTerms::privacyPolicy()
 * @see AutoTerms::cookiePolicy()
 * @see AutoTerms::termsAndConditions()
 */
?>
<div>
    <?php get_the_ID(); ?>
    <div class="container">
        <div class="row">
            <div class="col-12 col-12-sm">
                <div class="donationBg">
                    <div>
                        <h2 class="<?=$termsPage ? 'termsTitle' : ''?>"><?php the_title() ?></h2>
                    </div>
<?php foreach ($posts as $key => $post):
     if ($key === 0){
        $content = apply_filters('the_content', $post->post_content);
        $content = str_replace(']]>', ']]&gt;', $content);
        echo $content;
        continue;
  }
     if ($key === 1):?>
        <div id="termsArchive">
        <h3><?= __('Arhiva', 'gfShopTheme');?></h3>
        <ul>
     <?php endif; ?>
    <li><a class="extendedLink" href="<?=get_permalink($post)?>"><?=$post->post_title?></a></li>
    <?php if (array_key_last($posts) === $key):?>
    <?php if ($key !== 0):?>
        </ul></div>
    <?php endif;?>
    <?php endif; endforeach?>
    </div></div></div></div></div>
