<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?=$heading?></title>
</head>
<body>
<div marginwidth="0" marginheight="0" style="padding:30px" background="#fff" xmlns:>
    <div id="m_7381794864204686862wrapper" dir="ltr" style="background-color:#fff;margin:0;padding:70px 0;width:100%">
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tbody>
            <tr>
                <td align="center" valign="top" style="background-color:#f7f7f7;">
                    <div id="m_7381794864204686862template_header_image"></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="800"
                           id="m_7381794864204686862template_container"
                           style="background-color:#ffffff;border:1px solid #dedede;border-radius:3px">
                        <tbody>
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                       id="m_7381794864204686862template_header"
                                       style="color:#ffffff;border-bottom:0;font-weight:bold;line-height:100%;vertical-align:middle;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;border-radius:3px 3px 0 0">
                                    <tbody>
                                    <tr>
                                        <td style="text-align:center;padding:30px;">
                                            <img src="https://superhero.rs/wp-content/uploads/2021/12/sh@10x-300x28-1.png"
                                                 alt="superhero logo">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="m_7381794864204686862header_wrapper"
                                            style="padding:36px 48px;display:block;background:#3e4243">
                                            <h1 style="font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:30px;font-weight:700;line-height:150%;margin:0;color:#ffffff;background-color:inherit;text-align:center;">
                                                <?=$heading?></h1>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" width="800"
                                       id="m_7381794864204686862template_body">
                                    <tbody>
                                    <tr>
                                        <td valign="top" id="m_7381794864204686862body_content"
                                            style="background-color:#ffffff">
                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td valign="top" style="padding:48px 48px 32px">
                                                        <div id="m_7381794864204686862body_content_inner">
                                                         <p><?=$innerText?></p>
                                                        <p><?=sprintf(__('Ažuirane podatke možete pogledati <a href="%s">ovde</a>', 'gfShopTheme'), $termPageUrl)?></p>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="10" cellspacing="0" width="600"
                           id="m_7381794864204686862template_footer">
                        <tbody>
                        <tr>
                            <td valign="top" style="padding:0;border-radius:6px">
                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td colspan="2" valign="middle" id="m_7381794864204686862credit"
                                            style="border-radius:6px;border:0;color:#8a8a8a;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:12px;line-height:150%;text-align:center;padding:24px 0"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="yj6qo"></div>
        <div class="adL"></div>
    </div>
    <div class="adL"></div>
</div>
</body>
</html>