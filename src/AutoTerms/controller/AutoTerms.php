<?php

namespace GfWpPluginContainer\AutoTerms\controller;

use GfWpPluginContainer\AutoTerms\Mailer\Mailer;

class AutoTerms
{
    public function init()
    {
        add_action('init', [$this, 'registerPostType']);
        add_action('init', [$this, 'registerTaxonomy']);
        $this->createShortcodes();
        $this->sendMailWhenTermsHaveBeenUpdated();
    }

    public function activate()
    {
        $this->createPages();
    }

    public function registerPostType()
    {
        register_post_type('terms',
            [
                'labels' => ['name' => __('Terms', 'gfShopPlugin'), 'singular_name' => __('Term', 'gfShopPlugin')],
                'public' => true,
                'has_archive' => false,
                'rewrite' => ['slug' => 'terms'],
                'show_in_rest' => true,
                'hierarchical' => false,
                'supports' => ['title', 'editor', 'thumbnail', 'taxonomies'],
                'taxonomies' => ['term_type']
            ]
        );
    }

    public function registerTaxonomy()
    {
        register_taxonomy('term_type', 'terms',
            [
                'labels' => [
                    'name' => __('Term Types', 'gfShopPlugin'),
                    'singular_name' => __('Term Type', 'gfShopPlugin')
                ],
                'public' => true,
                'hierarchical' => true,
                'show_ui' => true,
                'show_admin_column' => true,
                'show_in_rest' => true,
            ]
        );
    }

    private function createShortcodes()
    {
        add_shortcode('privacy_policy', [$this, 'privacyPolicy']);
        add_shortcode('cookie_policy', [$this, 'cookiePolicy']);
        add_shortcode('terms_and_conditions', [$this, 'termsAndConditions']);
    }

    public function privacyPolicy()
    {
        $posts = get_posts([
            'post_type' => 'terms',
            'tax_query' => [
                [
                    'taxonomy' => 'term_type',
                    'field' => 'slug',
                    'terms' => 'politika-privatnosti'
                ]
            ]
        ]);
        $termsPage = false;
        ob_start();
        include __DIR__ . '/../templates/termsPage.php';
        return ob_get_clean();
    }

    public function cookiePolicy()
    {
        $posts = get_posts([
            'post_type' => 'terms',
            'tax_query' => [
                [
                    'taxonomy' => 'term_type',
                    'field' => 'slug',
                    'terms' => 'politika-kolacica'
                ]
            ]
        ]);
        $termsPage = false;
        ob_start();
        include __DIR__ . '/../templates/termsPage.php';
        return ob_get_clean();
    }

    public function termsAndConditions()
    {
        $posts = get_posts([
            'post_type' => 'terms',
            'tax_query' => [
                [
                    'taxonomy' => 'term_type',
                    'field' => 'slug',
                    'terms' => 'opsti-uslovi-koriscenja'
                ]
            ]
        ]);
        $termsPage = true;
        ob_start();
        include __DIR__ . '/../templates/termsPage.php';
        return ob_get_clean();
    }

    private function sendMailWhenTermsHaveBeenUpdated()
    {
        add_action('post_updated', function ($postId, $post, $oldPost)
        {
            /** @var \WP_Post $post */
            if ($post->post_type === 'terms') {
                $postTaxonomy = get_the_terms($post, 'term_type');
                if (count($postTaxonomy) === 0) {
                    return;
                }
                switch ($postTaxonomy[0]->name) {
                    case 'Politika kolačića':
                        $heading = 'Došlo je do izmena u našoj politici kolačića';
                        $pageUrl = get_home_url() . '/politika-kolacica';
                        $innerText = '';
                        break;
                    case 'Politika privatnosti':
                        $heading = 'Došlo je do izmena u našoj politici privatnosti';
                        $pageUrl = get_home_url() . '/politika-privatnosti';
                        $innerText = '';
                        break;
                    case 'Opšti uslovi korišćenja':
                        $heading = 'Došlo je do izmena u našim opštim uslovima korišćenja';
                        $pageUrl = get_home_url() . '/opsti-uslovi-koriscenja';
                        $innerText = '';
                        break;
                }
                if (isset($heading, $pageUrl, $innerText)) {
                    $mailer = new Mailer();
                    $args = array(
                        'role'    => 'customer',
                        'orderby' => 'user_nicename',
                        'order'   => 'ASC'
                    );
                    $users = get_users($args);
                    foreach ($users as $user) {
                        $mailer->sendTermsUpdateEmail($user->user_email, $heading, $pageUrl, $heading, $innerText = '');
                    }
                }
            }
        }, 10, 3);
    }
}