<?php


namespace GfWpPluginContainer\Wp;


use GF\Corvus\Subscription\Controller\SubscriptionController;
use GF\Corvus\Token\Controller\TokenController;

class Donation
{
    /**
     * @var int
     */
    private $donationId;


    public function init()
    {
        add_action('plugins_loaded', [$this, 'addDonationProduct']);
        add_action('woocommerce_before_calculate_totals', [$this, 'limitDonationQuantity']);
        add_action('woocommerce_before_calculate_totals', [$this, 'setDonationPrice']);

        add_action('admin_post_validateDonationForm', [$this, 'validateDonationForm']);
        add_action('admin_post_nopriv_validateDonationForm', [$this, 'validateDonationForm']);
        add_action('woocommerce_order_status_completed', [$this, 'setDonationCount'], 1);
    }

    public function addDonationProduct()
    {
        $this->donationId = wc_get_product_id_by_sku('donacija');
        if ($this->donationId != 0) {
            return;
        }
        $donation = new \WC_Product();
        $donation->set_name('Donacija');
        $donation->set_price('0');
        $donation->set_sku('donacija');
        $donation->set_sold_individually(true);
        $donation->save();
    }

    public function setDonationPrice(\WC_Cart $cartObject)
    {
        $cartItems = $cartObject->cart_contents;
        $donationAmount = 500;
        if (isset($_POST['donationAmount'])) {
            $donationAmount = $_POST['donationAmount'];
            if ($donationAmount < 500) {
                $donationAmount = 500;
            }
        }
        if (!empty($cartItems)) {
            foreach ($cartItems as $key => $value) {
                if ($value['data']->get_id() === $this->donationId) {
                    if ($value['data']->get_price() !== 0) {
                        $value['data']->set_price((int)$donationAmount);
                    }
                }
            }
        }
    }

    public function limitDonationQuantity(\WC_Cart $cartObject)
    {
        $cartItems = $cartObject->cart_contents;
        if (!empty($cartItems)) {
            foreach ($cartItems as $key => $value) {
                if ($value['data']->get_id() === $this->donationId) {
                    $cartObject->set_quantity($key, 1);
                }
            }
        }
    }

    public function validateDonationForm(): void
    {
        $donationAmount = $_POST['donationAmount'] ?? null;
        $donationFirstName = $_POST['donationFirstName'] ?? null;
        $donationLastName = $_POST['donationLastName'] ?? null;
        $donationEmail = $_POST['donationEmail'] ?? null;
        $errorInfo = [];
        $valid = true;
        $loggedInUser = is_user_logged_in();
        if ($_POST['recurringPayment'] !== '0') {
            if ($loggedInUser) {
                $subscriptionController = new SubscriptionController();
                if ($subscriptionController->doesUserHaveSubscription(get_current_user_id())) {
                    $valid = false;
                    $errorInfo['subscriptionError'] = __('Već imate aktivnu mesečnu donaciju', 'gfShopTheme');
                }
            } else {
                $valid = false;
                $errorInfo['subscriptionError'] = __('Morate biti prijavljeni da bi ste aktivirali mesečnu donaciju',
                    'gfShopTheme');
            }
        }

        if (!$donationAmount) {
            $errorInfo['donationAmount'] = __('Morate uneti iznos donacije', 'gfShopTheme');
            $valid = false;
        } else {
            if ($donationAmount < 500) {
                $errorInfo['donationAmount'] = __('Iznos donacije mora biti veći od 500 RSD', 'gfShopTheme');
                $valid = false;
            }
        }
        if (!$donationFirstName) {
            $errorInfo['donationFirstName'] = __('Morate uneti ime', 'gfShopTheme');
            $valid = false;
        }
        if (!$donationLastName) {
            $errorInfo['donationLastName'] = __('Morate uneti prezime', 'gfShopTheme');
            $valid = false;
        }
        if (!$donationEmail) {
            $errorInfo['donationEmail'] = __('Morate uneti email', 'gfShopTheme');
            $valid = false;
        } else {
            if (!filter_var($donationEmail, FILTER_VALIDATE_EMAIL)) {
                $errorInfo['donationEmail'] = __('Email nije validan', 'gfShopTheme');
                $valid = false;
            }
        }
        if (!$valid) {
            $errorInfo['error'] = '1';
            $url = get_home_url() . '/doniraj-uplatom/';
            wp_redirect(add_query_arg($errorInfo, $url));
            exit();
        }
        $user = wp_get_current_user();
        $order = wc_create_order();
        if ($user->ID !== 0) {
            $customer = new \WC_Customer($user->ID);
            $order->set_customer_id($customer->get_id());
            $order->set_address($customer->get_billing());
        }
        $order->add_product(wc_get_product(wc_get_product_id_by_sku('donacija')));
        $order->set_currency(get_woocommerce_currency());
        $order->set_customer_ip_address(\WC_Geolocation::get_ip_address());
        $order->set_customer_user_agent(wc_get_user_agent());
        $order->set_status('pending_payment');
        $order->update_meta_data('gfOrderType', 'donation');
        $order->set_billing_first_name($_POST['donationFirstName']);
        $order->set_billing_last_name($_POST['donationLastName']);
        $order->set_billing_email($_POST['donationEmail']);
        $gfPaymentType = 'donationStandard';
        $sub = '';
        if ($_POST['recurringPayment'] === '1') {
            $gfPaymentType = 'donationSub';
            $sub = 'Sub';
        }
        if (isset($_POST['payWithSavedCard']) && $_POST['payWithSavedCard'] === '1' && $loggedInUser) {
            $gfPaymentType = 'paymentWithToken' . $sub;
            $token = null;
            $userDefaultToken = get_user_meta(get_current_user_id(), 'defaultTokenId', true);
            $tokenController = new TokenController();
            if ($userDefaultToken) {
                $token = $tokenController->getByTokenId((int)$userDefaultToken);
            }
            if (!$token) {
                $gfPaymentType = 'donationStandard' . $sub;
            }
        }
        $order->update_meta_data('gfPaymentType', $gfPaymentType);
        if (isset($_POST['anonymousDonation']) && $_POST['anonymousDonation'] === '1') {
            $order->update_meta_data('anonymousDonation', '1');
        }
        $order->update_meta_data('subNumberOfMonths', $_POST['numberOfMonths']);
        $donation = array_values($order->get_items())[0];
        $donation->set_subtotal((int)$_POST['donationAmount']);
        $donation->set_total((int)$_POST['donationAmount']);

        $order->calculate_totals();
        $payment_gateways = WC()->payment_gateways()->get_available_payment_gateways();
        if (isset($payment_gateways['wcCorvusPayment'])) {
            /** @var \WcCorvusPayment $corvus */
            $corvus = $payment_gateways['wcCorvusPayment'];
            $order->set_payment_method($corvus);
            $orderId = $order->save();
            WC()->session = new \WC_Session_Handler();
            WC()->session->init();
            WC()->session->set('order_awaiting_payment', $orderId);
            $result = $corvus->process_payment($orderId);

            // Redirect to success/confirmation/payment page.
            if (isset($result['result']) && 'success' === $result['result']) {
                $result = apply_filters('woocommerce_payment_successful_result', $result, $orderId);

                if (!is_ajax()) {
                    wp_redirect($result['redirect']);
                    exit;
                }
                wp_send_json($result);
            }
        }
    }

    /**
     * @return int
     */
    private function getDonationsCount(): int
    {
        $donationProduct = wc_get_product(wc_get_product_id_by_sku('donacija'));
        $donationCount = 0;
        if ($donationProduct) {
            $donationCount = (int)$donationProduct->get_meta('donationCount');
            if ($donationCount > 0) {
                return $donationCount;
            }
        }
        return $donationCount;
    }

    /**
     * @param $orderId
     */
    public function setDonationCount($orderId): void
    {
        $order = wc_get_order($orderId);
        if ($order && $order->get_meta('gfOrderType') !== 'payment card add') {
            $order->update_meta_data('donationNumber', $this->getDonationsCount() + 1);
            $order->save();
            $donationProduct = wc_get_product(wc_get_product_id_by_sku('donacija'));
            $donationProduct->update_meta_data('donationCount', $this->getDonationsCount() + 1);
            $donationProduct->save();

        }
    }
}