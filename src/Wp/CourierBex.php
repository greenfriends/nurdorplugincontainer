<?php

namespace GfWpPluginContainer\Wp;

class CourierBex
{

    public function __construct()
    {
    }

    /**
     * @param int $orderId
     * @return array
     */
    private function prepareParamsForOrder(int $orderId): array
    {
        $order = wc_get_order($orderId);
        return [
            'shipmentId' => 0,
            'serviceSpeed' => 1,
            'shipmentType' => 1,
            'shipmentCategory' => 7,
            'shipmentWeight' => 0,
            'totalPackages' => 1,
            'invoiceAmount' => 0,
            'shipmentContents' => 11,
            'commentPublic' => '',
            'commentPrivate' => '',
            'personalDelivery' => true,
            'returnSignedInvoices' => true,
            'returnSignedConfirmation' => true,
            'returnPackage' => false,
            'payType' => 6,
            'insuranceAmount' => 0,
            'payToSender' => 0,
            'payToSenderViaAccount' => false,
            'sendersAccountNumber' => '',
            'bankTransferComment' => '',
            //@todo check how to send client id when name type is 3
            'tasks' => [
                [
                    'type' => 1,
                    'nameType' => 3,
                    'clientId' => 1234123,
                    'preNotification' => 0,
                    'comment' => ''
                ],
                [
                    'type' => 2,
                    'nameType' => 2,
                    'name1' => $order->get_billing_last_name(),
                    'name2' => $order->get_billing_first_name(),
                    'taxId' => '',
                    'addressType' => 1,
                    'municipalites' => '',
                    'place' => $order->get_billing_city(),
                    'street' => $order->get_billing_address_1(),
                    'houseNumber' => 0,
                    'apartment' => '',
                    'contactPerson' => $order->get_billing_last_name(). ' '. $order->get_billing_last_name(),
                    'phone' => $order->get_billing_phone(),
                    'date' => '',
                    'timeFrom' => '',
                    'timeTo' => '',
                    'preNotification' => 60,
                    'comment' => ''
                ],
                'reports' => [
                    'mode' => 1,
                    'type' => 1,
                    'adress' => 'email'
                ]
            ]
        ];
    }

    /**
     * @param array $orderIds
     * @return array
     */
    public function prepareRequestParams(array $orderIds): array
    {
       $params = [
           'shipmentslist' => [],
           'lang' => 2,
           'cust' => 101
       ];
       foreach ($orderIds as $orderId) {
           $params['shipmentslist'] = $this->prepareParamsForOrder($orderId);
       }
       return $params;
    }
}