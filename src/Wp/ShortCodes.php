<?php

namespace GfWpPluginContainer\Wp;

/**
 * Class ShortCodes.
 *
 * An integrator class, used to auto register short codes, based on a config file located in template.
 * These shortcodes are basically meant to be used as self contained simple php script that can be placed
 * anywhere by user via widget positioning.
 *
 * 'Example shortcode' =>
 *   [
 *       'name' => 'my_shortcode',
 *       'template' => 'myTemplateName'
 *   ],
 *
 * @package GfWpPluginContainer\Wp
 */
class ShortCodes
{

    public static function registerShortCode($name, $template)
    {
        add_shortcode($name, function ($atts) use ($template) {
            $data = $atts;
            if (file_exists($template) && !is_admin()) {
                if (!isset($_GET['_locale'])) {
                    ob_start();
                    include $template;
                    return ob_get_clean();
                }
            }
            return null;
        });
    }

    public static function setupShortCode($templateDir, $templateDirUrl, array $shortCodeConfig)
    {
        $template = $templateDir . '/' . $shortCodeConfig['template'] . '/view/' . $shortCodeConfig['template'] . '.php';
        $templateCss = $templateDirUrl . '/' . $shortCodeConfig['template'] . '/css/' . $shortCodeConfig['template'] . '.css';;
        $templateJs = $templateDirUrl . '/' . $shortCodeConfig['template'] . '/js/' . $shortCodeConfig['template'] . '.js';
        self::registerShortCode($shortCodeConfig['name'], $template);
        if (file_exists($templateDir . '/' . $shortCodeConfig['template'] . '/css/' . $shortCodeConfig['template'] . '.css')) {
            WpEnqueue::addFrontendStyle($shortCodeConfig['template'], $templateCss);
        }
        if (file_exists($templateDir . '/' . $shortCodeConfig['template'] . '/js/' . $shortCodeConfig['template'] . '.js')) {
            WpEnqueue::addFrontendScript($shortCodeConfig['template'], $templateJs);
        }

    }

    public static function setupShortCodes($templateDir, $templateDirUri, array $shortCodesConfig)
    {
        foreach ($shortCodesConfig as $shortCodeConfig) {
            $template = $templateDir . '/' . $shortCodeConfig['template'] . '/view/' . $shortCodeConfig['template'] . '.php';
            $templateCss = $templateDirUri . '/' . $shortCodeConfig['template'] . '/css/' . $shortCodeConfig['template'] . '.css';
            $templateJs = $templateDirUri . '/' . $shortCodeConfig['template'] . '/js/' . $shortCodeConfig['template'] . '.js';
            self::registerShortCode($shortCodeConfig['name'], $template);

            if (file_exists($templateDir . '/' . $shortCodeConfig['template'] . '/css/' . $shortCodeConfig['template'] . '.css')) {
                WpEnqueue::addFrontendStyle($shortCodeConfig['template'], $templateCss);
            }
            if (file_exists($templateDir . '/' . $shortCodeConfig['template'] . '/js/' . $shortCodeConfig['template'] . '.js')) {
                WpEnqueue::addFrontendScript($shortCodeConfig['template'], $templateJs);
            }
        }
    }
}