<?php

namespace GfWpPluginContainer\Wp;

/**
 * Class Sidebar.
 *
 * An integrator class, used to auto register sidebars based on a config file located in theme.
 *
 * @package GreenFriends\Setup
 */
class Sidebar
{
    /**
     * @var array
     */
    private $sidebarConfig;

    /**
     * Sidebar constructor.
     */
    public function __construct()
    {
        $this->sidebarConfig    = require PARENT_THEME_DIR . '/config/sidebars.php';
        $this->registerSidebars($this->sidebarConfig);
    }

    /**
     * @param array $sidebarArgs
     * 'name'          => ''  Sidebar Name. Required,
     * 'id'            => '' Sidebar Id. Required,
     * 'before_widget' => '' Html to display before widget default <div>,
     * 'after_widget'  => '' Html to display after widget default </div>,
     * 'before_title'  => '' Html to display before widget title default <h2 class="rounded">,
     * 'after_title'   => '' Html to display after widget title default </h2>,
     *
     */
    private function registerSidebar( $sidebarArgs )
    {
        $this->actionWidgetsInit( function () use ( $sidebarArgs ) {
            register_sidebar( $sidebarArgs );
        } );
    }

    /**
     * @param $sidebarsConfig
     */
    private function registerSidebars($sidebarsConfig )
    {
        foreach ( $sidebarsConfig as $sidebarArgs ) {
            $this->registerSidebar( $sidebarArgs );
        }
    }

    /**
     * @param $function
     */
    private function actionWidgetsInit($function )
    {
        add_action( 'widgets_init', function () use ( $function ) {
            $function();
        } );
    }
}
