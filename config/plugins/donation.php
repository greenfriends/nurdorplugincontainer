<?php

return [
    'settingPage' => [
        'pageTitle' => 'Donation settings',
        'menuTitle' => 'Donation',
        'capability' => 'manage_options',
        'menuSlug' => 'donation_settings',
        'template' => 'donation'
    ],
    'registerSettings' => [
        'optionGroup' => 'gf_donation_options',
        'options' => [
            'gf_donation_options' => []
        ]
    ],
    'shortCode' => [
        'name' => 'donation_form',
        'template' => 'donationForm'
    ],
];
