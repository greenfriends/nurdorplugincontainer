<?php
if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
}

settings_errors();

$options = get_option('gf_donation_options');
$activeDonationValues = $options['activeDonationValues'] ?? [];
//$donationAmounts = $options['donationAmounts'] ?? [];
$productDonations = $options['productDonations'] ?? [];

$l = isset($productDonations) ? count($productDonations) : 0;
//$i = isset($donationAmounts) ? count($donationAmounts) : 0;
?>
<div class="wrap">
    <form method="post" class="donation-form" action="options.php">
        <?php
        settings_fields('gf_donation_options');
        do_settings_sections('gf_donation_options');
        ?>
        <h1><?= __('Podešavanja donacije', 'gfShopTheme') ?></h1>
        <div class="donationInputWrapper">
<!--        <div class="donationInputWrapper" data-counter="--><?//= $i ?><!--">-->
<!--            <label class="titleLabel">--><?//= __('Dodajte iznos proizvoljne donacije', 'gfShopTheme') ?>
<!--                <button id="addDonationInput">&plus;</button>-->
<!--            </label>-->
<!--            <span class="text-muted">--><?php //_e('Prvi unos na listi će biti uzet kao minimalni iznos donacije, lista će se korisniku prikazivati istim redosledom kao i ovde','gfShopTheme')?><!--</span>-->
            <?php
            $k = 0;
//            foreach ($donationAmounts as $donationAmount) {
//                printf('<label><input class="donationInput" type="number" name="gf_donation_options[donationAmounts][%s]"
//                        value="%s"><button class="removeDonationInput">&minus;</button></label>', $k, $donationAmount);
//                $k++;
//            }
            ?>

            <template id="donationInput">
                <label><input class="donationInput" type="number" name="gf_donation_options[donationAmounts]" value="">
                    <button class="removeDonationInput">&minus;</button>
                </label>
            </template>
            <template id="donationProductInput">
                <label><input class="donationInput" type="number" name="gf_donation_options[productDonations]" value="">
                    <button class="removeDonationInput">&minus;</button>
                </label>
            </template>
        </div>
        <h2><?= __('Štiklirajte opcije donacije koje želite da koristite', 'gfShopTheme') ?></h2>
        <div>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Jedan mesec]"
                          value="1" <?php if (isset($activeDonationValues['Jedan mesec'])) {
                    echo 'checked';
                } ?>><?= __('Jedan mesec', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Dva meseca]"
                          value="2" <?php if (isset($activeDonationValues['Dva meseca'])) {
                    echo 'checked';
                } ?>><?= __('Dva meseca', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Tri meseca]"
                          value="3" <?php if (isset($activeDonationValues['Tri meseca'])) {
                    echo 'checked';
                } ?>><?= __('Tri meseca', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Četiri meseca]"
                          value="4" <?php if (isset($activeDonationValues['Četiri meseca'])) {
                    echo 'checked';
                } ?>><?= __('Četiri meseca', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Pet meseci]"
                          value="5" <?php if (isset($activeDonationValues['Pet meseci'])) {
                    echo 'checked';
                } ?>><?= __('Pet meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Šest meseci]"
                          value="6" <?php if (isset($activeDonationValues['Šest meseci'])) {
                    echo 'checked';
                } ?>><?= __('Šest meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Sedam meseci]"
                          value="7" <?php if (isset($activeDonationValues['Sedam meseci'])) {
                    echo 'checked';
                } ?>><?= __('Sedam meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Osam meseci]"
                          value="8" <?php if (isset($activeDonationValues['Osam meseci'])) {
                    echo 'checked';
                } ?>><?= __('Osam meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Devet meseci]"
                          value="9" <?php if (isset($activeDonationValues['Devet meseci'])) {
                    echo 'checked';
                } ?>><?= __('Devet meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Deset meseci]"
                          value="10" <?php if (isset($activeDonationValues['Deset meseci'])) {
                    echo 'checked';
                } ?>><?= __('Deset meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Jedanaest meseci]"
                          value="11" <?php if (isset($activeDonationValues['Jedanaest meseci'])) {
                    echo 'checked';
                } ?>><?= __('Jedanaest meseci', 'gfShopTheme') ?> </label>
            <label><input type="checkbox" name="gf_donation_options[activeDonationValues][Dvanaest meseci]"
                          value="12" <?php if (isset($activeDonationValues['Dvanaest meseci'])) {
                    echo 'checked';
                } ?>><?= __('Dvanaest meseci', 'gfShopTheme') ?> </label>
        </div>

        <div class="donationProductWrapper" data-counter="<?=$l?>">
            <label class="titleLabel"><?= __('Dodajte iznos donacije proizvoda', 'gfShopTheme') ?>
                <button id="addProductDonationInput">&plus;</button>
            </label>
            <?php
            $j = 0;
            foreach ($productDonations as $productDonation) {
                printf('<label><input class="donationInput" type="number" name="gf_donation_options[productDonations][%s]" 
                        value="%s"><button class="removeDonationInput">&minus;</button></label>', $j, $productDonation);
                $j++;
            }
            ?>
        </div>
        <?php submit_button(); ?>
    </form>
</div>

