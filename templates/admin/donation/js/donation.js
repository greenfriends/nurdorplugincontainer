jQuery(document).ready(function ($) {
    if ($('.donation-form').length > 0) {

        $('#addDonationInput').click(function (e) {
            e.preventDefault();
            var container = document.querySelector(".donationInputWrapper");
            var valuesCounter = Number(container.dataset.counter);

            if ('content' in document.createElement('template')) {
                var template = document.querySelector('#donationInput').content.cloneNode(true);
                template.querySelector('.donationInput').name = 'gf_donation_options[donationAmounts][' + valuesCounter + ']';
                valuesCounter++;
                container.dataset.counter = String(valuesCounter);
                container.appendChild(template);
                $('.removeDonationInput').click(function (e) {
                    e.preventDefault();
                    $(this).parent().remove()
                })
            } else {
                console.error('Your browser does not support templates');
            }
        });

        $('#addProductDonationInput').click(function(e) {
            e.preventDefault();
            var containerProduct = document.querySelector('.donationProductWrapper');
            var valuesCounterProduct = Number(containerProduct.dataset.counter);

            if('content' in document.createElement('template')) {
                var templateProduct = document.querySelector('#donationProductInput').content.cloneNode(true);
                templateProduct.querySelector('.donationInput').name = 'gf_donation_options[productDonations][' + valuesCounterProduct + ']';
                valuesCounterProduct++;
                containerProduct.dataset.counter = String(valuesCounterProduct);
                containerProduct.appendChild(templateProduct);
                $('.removeDonationInput').click(function (e) {
                    e.preventDefault();
                    $(this).parent().remove()
                })
            } else {
                console.error('Your browser does not support templates');
            }
        })

        $('.removeDonationInput').click(function (e) {
            e.preventDefault();
            $(this).parent().remove()
        })
    }
});