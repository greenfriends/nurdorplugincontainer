<li class="slider-wrapper">
    <div class="row sliderControls">
        <div class="col">
            <h4 data-slider-name="<?= $data['sliderName'] ?>"
                class="slider-heading"><?php _e('Ime slajdera: ', 'gfShopTheme'); ?><?= $data['sliderName'] ?></h4>
        </div>
        <div class="col">
            <h4><?php _e('Shortcode:  [slider name="' . $data['sliderName'] . '"]', 'gfShopTheme') ?></h4>
        </div>
        <div class="col">
            <button class="delete-slider btn btn-danger float-right"><?php _e('Izbriši slajder', 'gfShopTheme'); ?></button>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-1">
            <label for="image_width"><?php _e('Širina slike', 'gfShopTheme'); ?></label>
            <input type="text" name="gf_slider_options[sliders][<?= $data['sliderName'] ?>][image_width]"
                   class="form-control images-width-input"
                   value="<?= $data['sliderData']['image_width'] ?>"/>
        </div>
        <div class="form-group col-md-1">
            <label for="image_height"><?php _e('Visina slike', 'gfShopTheme'); ?></label>
            <input type="text" name="gf_slider_options[sliders][<?= $data['sliderName'] ?>][image_height]"
                   class="form-control images-height-input"
                   value="<?= $data['sliderData']['image_height'] ?>"/>
        </div>
        <div class="form-group col-md-1">
            <label for="slide_speed"><?php _e('Brzina slajdera', 'gfShopTheme'); ?></label>
            <input type="text" name="gf_slider_options[sliders][<?= $data['sliderName'] ?>][slide_speed]"
                   class="form-control images-slide-speed-input"
                   value="<?= $data['sliderData']['slide_speed'] ?>"/>
        </div>
    </div>
    <h4>Images</h4>
    <?php
    $imageCountWithData = 0;
    foreach ($data['sliderData']['images'] as $image) {
        if (strlen($image['url']) === 0) {
            continue;
        }
        $imageCountWithData++;
    }
    ?>
    <ul data-image-count="<?= $imageCountWithData ?>"
        class="list-unstyled list-inline sortable images-list-<?= $data['sliderName'] ?>">
        <?php
        if (isset($data['sliderData']['images']) && count($data['sliderData']['images']) > 0) {
            $i = 0;
            foreach ($data['sliderData']['images'] as $image) {
                if (strlen($image['url']) === 0) {
                    continue;
                }
                $imageUrl = $image['url'];
                $subImages = $image['sub-image'] ?? [];
                $subImagesCount = count($subImages);
                $subWrapTitle = $subImagesCount > 0 ? '<h3>' . __('Pod-slajderi', 'gfShopTheme') . '</h3>' : '';
                $sliderName = $data['sliderName'];
                $imageLink = $image['linkTo'];
                $title = $image['title'];
                $text = $image['text'];
                $buttonText = $image['buttonText'];
                $linkToInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][linkTo]';
                $titleInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][title]';
                $textInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][text]';
                $buttonInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][buttonText]';
                echo '
                <li class="list-inline-item">
                 <div class="inputSubWrapper">
                               <label for="'. $titleInput .'"><b>' .__('Ime slajda', 'gfShopTheme') . '</b></label>
                         <textarea name="' . $titleInput . ' " class="image-title-input"' . '
                               placeholder=" ' . __('Unesite naziv', 'gfShopTheme') . '">' . $title . '</textarea>
                               </div>
                        <div class="image-preview-wrapper">
                        <img class="image-preview" src="' . $imageUrl . '" alt="" height="200px" width="200px">
                        <div class="remove-image btn btn-danger"><span>&times</span></div>
                                <div class="inputSubWrapper textWrapper">
                               <label for="'. $textInput .'"><b>' .__('Tekst slajda', 'gfShopTheme') . '</b></label>
                        <textarea rows="10" class="image-text-input" name="' . $textInput . '" placeholder=" ' . __('Unesite tekst', 'gfShopTheme') . '">' . $text . '</textarea>   
                        </div>
                    </div>
           
                    <div class="inputs-wrapper">
                    <input type="hidden" name="gf_slider_options[sliders][' . $sliderName . '][images][' . $i . '][url]" class="image-url-input"
                          value="' . $imageUrl . '"/>
                          <div class="inputSubWrapper">
                          <label for="' . $linkToInput . '"><b>' . __('Proizvoljan link slike', 'gfShopTheme') . '</b></label>
                        <input type="text" name="' . $linkToInput . ' " class="image-custom-link-input" value="' . $imageLink . '"
                               placeholder=" ' . __('Unesite proizvoljan link za sliku', 'gfShopTheme') . '"/>
                               </div>                            
                        <div class="inputSubWrapper textWrapper">
                        <label for="' . $buttonInput . '"><b>' . __('Tekst dugmeta', 'gfShopTheme') . '</b></label>
                        <input type="text" name="' . $buttonInput .'" class="button-title-input" value="' . $buttonText .'" placeholder="' . __('Unesite tekst dugmeta', 'gfShopTheme') . '">
                        </div>
                    </div>' . $subWrapTitle .  '
                 <ul class="subItems">';
                if($subImagesCount > 0) {
                    $k = 0;
                    foreach($subImages as $subImage) {
                        $subImageUrl = $subImage['url'];
                        $subImageLink = $subImage['linkTo'];
                        $subImageTitle = $subImage['title'];
                        $subImageText = $subImage['text'];
                        $subImageButtonText = $subImage['buttonText'];
                        $subLinkToInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][sub-image][' . $k . '][linkTo]';
                        $subTitleInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][sub-image][' . $k . '][title]';
                        $subTextInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][sub-image][' . $k . '][text]';
                        $subButtonInput = 'gf_slider_options[sliders][' . $data['sliderName'] . '][images][' . $i . '][sub-image][' . $k . '][buttonText]';
                        echo '
                     <li class="list-inline-item-sub">
        <div class="inputSubWrapper">
            <label for="' . $subTitleInput .'"><b>' .  __('Ime slajda', 'gfShopTheme')  . '</b></label>
            <textarea  name="' . $subTitleInput .'" class="image-title-input"
                       placeholder="' . __('Unesite naziv', 'gfShopTheme')  . '">' . $subImageTitle . '</textarea>
        </div>
        <div class="image-preview-wrapper">
            <img class="image-preview" src="' . $subImageUrl .'" alt="" height="200px" width="200px">
            <div class="remove-image btn btn-danger"><span>&times</span></div>
            <div class="inputSubWrapper textWrapper">
                <label for="' . $subTextInput .'"><b>' . __('Tekst dugmeta', 'gfShopTheme') . '</b></label>
                <textarea rows="10" class="image-text-input" name="' . $subTextInput .'"
                          placeholder="' .  __('Unesite tekst', 'gfShopTheme') . '">' . $subImageText .'</textarea>
            </div>
        </div>
        <div class="inputs-wrapper">
            <input type="hidden" name="gf_slider_options[sliders][' . $sliderName . '][images][' . $i . '][sub-image][' . $k .'][url]" class="image-url-input"
                   value="' . $subImageUrl .'"/>
            <div class="inputSubWrapper">
                <label for="' . $subLinkToInput .'"><b>' . __('Proizvoljan link slike', 'gfShopTheme') . '</b></label>
                <input type="text" name="' . $subLinkToInput .'" class="image-custom-link-input" value="' . $subImageLink .'"
                       placeholder="' .  __('Unesite proizvoljan link slike', 'gfShopTheme') . '"/>
            </div>

            <div class="inputSubWrapper textWrapper">
                <label for="' . $subButtonInput .'"><b>' .  __('Tekst slajda', 'gfShopTheme')  . '</b></label>
                <input type="text" name="' . $subButtonInput .'" class="button-title-input" value="' . $subImageButtonText .'" placeholder="' . __('Unesite tekst dugmeta', 'gfShopTheme') . '">
            </div>
        </div>
    </li>
                ';
                        $k++;
                    }
                }
                echo '</ul></li>';
                $i++;
            }

        } ?>
    </ul>
    <button class="btn btn-primary upload-images"
    ><?php _e('Izaberite slike', 'gfShopTheme'); ?></button>
    <button class="btn btn-primary save-slider" type="submit"><?= __('Sačuvaj slajder', 'gfShopTheme')?></button>
</li>