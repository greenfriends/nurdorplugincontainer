<?php
if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
}
?>
<div class="wrap">
    <?php settings_errors(); ?>
    <div class="row">
        <div class="col">
            <h1>Gf sliders</h1>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="input-group">
                    <input class="slider-name-input" type="text"
                           placeholder="<?php _e('Unesite ime slajdera', 'gfShopTheme'); ?>"
                           value="">
                    <button id="slider-add-button"
                            class="btn btn-primary d-inline"><?php _e('Dodaj slajder', 'gfShopTheme'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <form method="post" class="container-fluid" action="options.php">
        <?php settings_fields('gf_slider_options'); ?>
        <?php do_settings_sections('gf_slider_options'); ?>
        <?php $options = get_option('gf_slider_options'); ?>
        <ul id="forms-container" class="list-unstyled">
            <?php
            if (isset($options['sliders']) && count($options['sliders']) > 0) {
                $formsCount = count($options['sliders']);
                foreach ($options['sliders'] as $key => $value) {
                    $empty = true;
                    $data = [
                        'sliderName' => $key,
                        'sliderData' => $value,
                    ];
                    foreach ($data['sliderData']['images'] as $image) {
                        if (strlen($image['url']) > 0) {
                            $empty = false;
                        }
                    }
                    if ($empty) {
                        continue;
                    }
                    $formsCount++;
                    \GfWpPluginContainer\GfShopThemePlugins::getTemplatePartials('admin', 'slider', 'sliderForm', $data);
                }
            } ?>
        </ul>
        <?php submit_button(); ?>
    </form>
</div>

<template id="slider-form-template">
    <li class="slider-wrapper">
        <div class="row">
            <div class="col">
                <h4 data-slider-name="" class="slider-heading"><?php _e('Slider name: ', 'gfShopTheme'); ?></h4>
            </div>
            <div class="col">
                <button class="delete-slider btn btn-danger float-right"><?php _e('Delete slider', 'gfShopTheme'); ?></button>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-1">
                <label for="image_width"><?php _e('Širina slike', 'gfShopTheme'); ?></label>
                <input type="text" name=""
                       class="form-control images-width-input"
                       value=""/>
            </div>
            <div class="form-group col-md-1">
                <label for="image_height"><?php _e('Visina slike', 'gfShopTheme'); ?></label>
                <input type="text" name=""
                       class="form-control images-height-input"
                       value=""/>
            </div>
            <div class="form-group col-md-1">
                <label for="slide_speed"><?php _e('Brzina slajdera', 'gfShopTheme'); ?></label>
                <input type="text" name=""
                       class="form-control images-slide-speed-input"
                       value=""/>
            </div>
        </div>
        <h4 class="slideImagesTitle">Images</h4>
        <ul class="list-unstyled list-inline sortable images-list">

        </ul>
        <button class="btn btn-primary upload-images"><?php _e('Izaberite slike', 'gfShopTheme'); ?></button>
        <button class="btn btn-primary save-slider" type="submit"><?= __('Sačuvaj slajder', 'gfShopTheme') ?></button>
    </li>
</template>

<template id="image-template">
    <li class="list-inline-item">
        <div class="inputSubWrapper">
            <label for=""><b><?= __('Ime slajda', 'gfShopTheme') ?></b></label>
            <textarea  name="" class="image-title-input"
                      placeholder="<?= __('Unesite naziv', 'gfShopTheme') ?>"></textarea>
        </div>
        <div class="image-preview-wrapper">
            <img class="image-preview" src="" alt="" height="200px" width="200px">
            <div class="remove-image btn btn-danger"><span>&times</span></div>
            <div class="inputSubWrapper textWrapper">
                <label for=""><b><?= __('Tekst slajda', 'gfShopTheme') ?></b></label>
                <textarea rows="10" class="image-text-input" name=""
                          placeholder="<?= __('Unesite tekst', 'gfShopTheme') ?>"></textarea>
            </div>
        </div>
        <div class="inputs-wrapper">
            <input type="hidden" name="" class="image-url-input"
                   value=""/>
            <div class="inputSubWrapper">
                <label for=""><b><?= __('Proizvoljan link slike', 'gfShopTheme') ?></b></label>
                <input type="text" name="" class="image-custom-link-input" value=""
                       placeholder="<?= __('Unesite proizvoljan link slike', 'gfShopTheme') ?>"/>
            </div>

            <div class="inputSubWrapper textWrapper">
                <label for=""><b><?=__('Tekst dugmeta', 'gfShopTheme')?></b></label>
                <input type="text" name="" class="button-title-input" value="" placeholder="<?=__('Unesite tekst dugmeta', 'gfShopTheme')?>">
            </div>
        </div>
        <buttton class="btn btn-primary upload-sub-images"><?= __('Izaberite slike unutar postojećeg slajdera', 'gfShopTheme')?></buttton>
        <h3><?= __('Pod-slajderi', 'gfShopTheme') ?></h3>
        <div class="subItems"></div>
    </li>
</template>
<template id="sub-image-template">
    <li class="list-inline-item-sub">
        <div class="inputSubWrapper">
            <label for=""><b><?= __('Ime slajda', 'gfShopTheme') ?></b></label>
            <textarea  name="" class="image-title-input"
                       placeholder="<?= __('Unesite naziv', 'gfShopTheme') ?>"></textarea>
        </div>
        <div class="image-preview-wrapper">
            <img class="image-preview" src="" alt="" height="200px" width="200px">
            <div class="remove-image btn btn-danger"><span>&times</span></div>
            <div class="inputSubWrapper textWrapper">
                <label for=""><b><?=__('Tekst dugmeta', 'gfShopTheme')?></b></label>
                <textarea rows="10" class="image-text-input" name=""
                          placeholder="<?= __('Unesite tekst', 'gfShopTheme') ?>"></textarea>
            </div>
        </div>
        <div class="inputs-wrapper">
            <input type="hidden" name="" class="image-url-input"
                   value=""/>
            <div class="inputSubWrapper">
                <label for=""><b><?= __('Proizvoljan link slike', 'gfShopTheme') ?></b></label>
                <input type="text" name="" class="image-custom-link-input" value=""
                       placeholder="<?= __('Unesite proizvoljan link slike', 'gfShopTheme') ?>"/>
            </div>

            <div class="inputSubWrapper textWrapper">
                <label for=""><b><?= __('Tekst slajda', 'gfShopTheme') ?></b></label>
                <input type="text" name="" class="button-title-input" value="" placeholder="<?=__('Unesite tekst dugmeta', 'gfShopTheme')?>">
            </div>
        </div>
    </li>
</template>