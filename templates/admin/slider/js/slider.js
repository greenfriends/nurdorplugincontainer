jQuery(document).ready(function ($) {
    var mediaUploader;
    var sliderName = null;
    $( ".sortable" ).sortable();
    $( ".sortable" ).disableSelection();
    $('#slider-add-button').click(function (e) {
        var sliderName = $('.slider-name-input').val().replace(/\s+/g, '-');
        printSliderForm(sliderName);
        $('.slider-name-input').val('');
    });

    function printAttachments(sliderName) {
        var attachments = mediaUploader.state().get('selection').toJSON();
        var container = document.querySelector(".images-list-" + sliderName);
        var imageCounter = Number (container.dataset.imageCount);

        for (var index in attachments) {
            if ('content' in document.createElement('template')) {
                var template = document.querySelector('#image-template').content.cloneNode(true);
                template.querySelector('.image-preview').src = attachments[index].url;
                template.querySelector('.image-custom-link-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageCounter + '][linkTo]';
                template.querySelector('.image-title-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageCounter + '][title]';
                template.querySelector('.image-text-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageCounter + '][text]';
                template.querySelector('.image-url-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageCounter + '][url]';
                template.querySelector('.button-title-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageCounter + '][buttonText]';
                template.querySelector('.image-url-input').value = attachments[index].url;
                imageCounter++;
                container.dataset.imageCount = String(imageCounter) ;
                container.appendChild(template);
            } else {
                console.error('Your browser does not support templates');
            }
        }
    }

    function printSubAttachments(clickedElem,sliderName) {
        let attachments = mediaUploader.state().get('selection').toJSON();
        let container = clickedElem.parentElement.querySelector('.subItems');
        let imageIndex = document.querySelector('.images-list-' + sliderName).dataset.imageCount - 1;
        let imageCounter = container.querySelectorAll('li').length;
        if(imageCounter === 0) {
            imageCounter = 0;
        }
        for(let index in attachments) {
            if ('content' in document.createElement('template')) {
                let template = document.querySelector('#sub-image-template').content.cloneNode(true);
                template.querySelector('.image-preview').src = attachments[index].url;
                template.querySelector('.image-custom-link-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageIndex + '][sub-image][' + imageCounter + '][linkTo]';
                template.querySelector('.image-title-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageIndex + '][sub-image][' + imageCounter + '][title]';
                template.querySelector('.image-text-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageIndex + '][sub-image][' + imageCounter + '][text]';
                template.querySelector('.image-url-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageIndex + '][sub-image][' + imageCounter + '][url]';
                template.querySelector('.button-title-input').name = 'gf_slider_options[sliders][' + sliderName + '][images][' + imageIndex + '][sub-image][' + imageCounter + '][buttonText]';
                template.querySelector('.image-url-input').value = attachments[index].url;
                imageCounter++;
                container.appendChild(template);
            } else {
                console.error('Your browser does not support templates');
            }
        }
    }

    function printSliderForm(sliderName) {
        if ('content' in document.createElement('template')) {
            var template = document.querySelector('#slider-form-template').content.cloneNode(true);
            template.querySelector('.images-list').dataset.imageCount = '0';
            template.querySelector('.images-list').className += '-' + sliderName;
            template.querySelector('.slider-heading').innerHTML += sliderName;
            template.querySelector('.slider-heading').dataset.sliderName = sliderName ;
            template.querySelector('.images-width-input').name = 'gf_slider_options[sliders][' + sliderName + '][image_width]';
            template.querySelector('.images-height-input').name = 'gf_slider_options[sliders][' + sliderName + '][image_height]';
            template.querySelector('.images-slide-speed-input').name = 'gf_slider_options[sliders][' + sliderName + '][slide_speed]';
            var container = document.querySelector('#forms-container');
            container.appendChild(template);
        } else {
            console.error('Your browser does not support templates');
        }
    }

    $(document).on('click', '.upload-images', function (e) {
        e.preventDefault();
        sliderName = $(this).parent().find('.slider-heading').data('sliderName');
        mediaUploader = wp.media({
            multiple: 'add'
        });
        mediaUploader.on('select', function () {
            printAttachments(sliderName);
        });
        mediaUploader.open();
    });
    $(document).on('click', '.upload-sub-images', function (e) {
        let clickedElem = e.target;
        e.preventDefault();
        sliderName = $(this).parent().parent().parent().find('.slider-heading').data('sliderName');
        mediaUploader = wp.media({
            multiple: 'add'
        });
        mediaUploader.on('select', () => {
            printSubAttachments(clickedElem,sliderName);
        });
        mediaUploader.open();
    })
    $(document).on('click', '.remove-image', function (e) {
        e.preventDefault();
        var inputsWrapper =  $(this).parent().next('.inputs-wrapper');

        $(inputsWrapper).find('input').each(function () {
            $(this).val('');
        });
        $(inputsWrapper).find('textarea').val('');
        $(this).parent().parent().remove();
    });
    $(document).on('click', '.delete-slider', function (e) {
        e.preventDefault();
        var parent = $(this).parent().parent().parent();
        parent.find('.slider-heading').data('sliderName');
        parent.find('input').each(function () {
            $(this).val("");
        });
        parent.hide();
    })
});

