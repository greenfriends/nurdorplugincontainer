jQuery(document).ready(function ($) {
    onClickPerm();
    customSelect();
    function customSelect() {
        // Cache the number of options
        var $this = $('.selection'),
            numberOfOptions = $('.selection').children('option').length;

        // Hides the select element
        $this.addClass('s-hidden');

        // Wrap the select element in a div
        $this.wrap('<div class="select"></div>');

        // Insert a styled div to sit over the top of the hidden select element
        $this.after('<div class="styledSelect"><i class="fas fa-chevron-down"></i></div>');

        // Cache the styled div
        var $styledSelect = $this.next('div.styledSelect');

        // Show the first select option in the styled div
        $styledSelect.text($this.children('option').eq(0).text()).append('<i class="fas fa-chevron-down"></i>');


        // Insert an unordered list after the styled div and also cache the list
        var $list = $('<ul />', {
            'class': 'options'
        }).insertAfter($styledSelect);

        // Insert a list item into the unordered list for each select option
        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        // Cache the list items
        var $listItems = $list.children('li');

        // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
        $styledSelect.click(function (e) {
            e.stopPropagation();
            $(this).toggleClass('active').next('ul.options').toggle('fast', 'swing');
        });

        // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
        // Updates the select element to have the value of the equivalent option
        $listItems.click(function (e) {
            e.stopPropagation();
            //$styledSelect;
            $styledSelect.text($(this).text()).append('<i class="fas fa-chevron-down"></i>').removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            /* alert($this.val()); Uncomment this for demonstration! */
        });

        // Hides the unordered list when clicking outside of it
        $(document).click(function () {
            $styledSelect.removeClass('active');
            $list.hide();
        });
    }

    function showPermAcc(button) {
        if ($(button).hasClass('permActive')) {
            $('.permanentAccount').addClass('show');
        } else {
            $('.permanentAccount').removeClass('show');
        }
    }

    function onClickPerm() {
        $('.btn').on('click', function (e) {
            e.preventDefault();
            if($(this).hasClass('disabledDonation')) {
                $('#loggedOutNoticeContainer').removeClass('hidden');
                return;
            }
            if($('#recurringDonation').hasClass('disabledDonation')) {
                if (e.target.id === 'oneTimeDonation') {
                    $('#loggedOutNoticeContainer').addClass('hidden');
                }
            }
            $(this).addClass('active').siblings('.btn').removeClass('active');
            showPermAcc(this);
        })
        showPermAcc('.btn.active');
    }

    $('#oneTimeDonation').on('click', function () {
        $('#recurringPayment').val(0)
    })
    $('#recurringDonation').on('click', function () {
        $('#recurringPayment').val(1)
    })

    let searchParams = new URLSearchParams(window.location.search);
    if (searchParams.has('error')) {
        $('html, body').animate({
            scrollTop: $("#donationForm").offset().top-200
        }, 500);
    }
})



