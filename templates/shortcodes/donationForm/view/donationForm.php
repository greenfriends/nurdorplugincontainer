<?php

use GF\Corvus\Subscription\Controller\SubscriptionController;
use GF\Corvus\Token\Controller\TokenController;

$donationId = wc_get_product_id_by_sku('donacija');
$firstName = '';
$lastName = '';
$email = '';
$options = get_option('gf_donation_options');
$numberOfMonths = isset($options['activeDonationValues']) ? $options['activeDonationValues'] : [];
$donationAmounts = isset($options['donationAmounts']) ? $options['donationAmounts'] : [0 => '500 ' . get_woocommerce_currency_symbol()];
$shouldSubBeActive = false;
$noticeMessage = __('Morate biti ulogovani da biste donirali na mesečnom nivou','gfShopTheme');
$noticeClass = 'hidden';
$last4 = '';
$token = null;
if (strlen($donationId) > 0):
    if (is_user_logged_in()) {
        $customer = WC()->customer;
        $firstName = $customer->get_billing_first_name();
        $lastName = $customer->get_billing_last_name();
        $email = $customer->get_billing_email();
        $shouldSubBeActive = true;
        $subscriptionController = new SubscriptionController();
        if ($subscriptionController->doesUserHaveSubscription($customer->get_id())) {
            $shouldSubBeActive = false;
            $noticeMessage = __('Već imate aktivnu mesečnu donaciju','gfShopTheme');
        }
        $userDefaultToken = get_user_meta(get_current_user_id(), 'defaultTokenId', true);
        $tokenService = new \GF\Corvus\Token\Service\Ajax();
        $tokenController = new TokenController();
        if ($userDefaultToken) {
            $token = $tokenController->getByTokenId((int)$userDefaultToken);
        }
        if ($token) {
            $last4 = '****' . substr($token->getCardNumber(), -4);
        }
    }
if (isset($_GET['subscriptionError'])) {
    $noticeMessage = $_GET['subscriptionError'];
    $noticeClass = '';
}
    ?>
    <div class="row">
        <div class="donationFormContainer">
            <?php
            /** @see \GfPluginsCore\Donation::validateDonationForm() */
            ?>
            <form id="donationForm" action="<?=esc_url(admin_url('admin-post.php'));?>" method="post" novalidate>
                <input type="hidden" name="action" value="validateDonationForm">
                <div class="donationForm">
                    <div class="row">
                        <div class="col-4 col-12-sm">
                            <?php if(isset($_GET['donationFirstName'])):?>
                            <span class="donationNotice"><?=__($_GET['donationFirstName'] . '*','gfShopTheme')?></span>
                            <?php endif;?>
                            <input aria-label="<?=__('Ime','gfShopTheme')?>" class="additionalInput" type="text" value="<?=$firstName?>" placeholder="<?php
                            _e('Ime', 'gfShopTheme') ?>" name="donationFirstName"/>
                        </div>
                        <div class="col-4 col-12-sm">
                            <?php if(isset($_GET['donationLastName'])):?>
                                <span class="donationNotice"><?=__($_GET['donationLastName'] . '*','gfShopTheme')?></span>
                            <?php endif;?>
                            <input aria-label="<?=__('Prezime','gfShopTheme')?>" class="additionalInput" type="text" value="<?=$lastName?>" placeholder="<?php
                            _e('Prezime', 'gfShopTheme') ?>" name="donationLastName"/>
                        </div>
                        <div class="col-12-sm show-div">
                            <!--					<input class="additionalInput" type="email" value="--><?
                                                                                                          //=$email
                                                                                                          ?><!--" placeholder="E-mail adresa" />-->
                        </div>
                        <div class="col-4 col-12-sm">
                            <?php if(isset($_GET['donationAmount'])):?>
                                <span class="donationNotice"><?=__($_GET['donationAmount'] . '*','gfShopTheme')?></span>
                            <?php endif;?>
                            <input id="donationAmountInput" class="additionalInput" type="number" name="donationAmount" aria-label="<?=__('Iznos donacije','gfShopTheme')?>" placeholder="<?=__('Iznos donacije','gfShopTheme')?>">
                            <span class="acceptedMinMoney"><?=
                                __('Minimalni iznos donacije je 500','gfShopTheme') . ' ' . get_woocommerce_currency_symbol();?>
                                </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8 col-12-sm">
                            <?php if(isset($_GET['donationEmail'])):?>
                                <span class="donationNotice"><?=__($_GET['donationEmail'] . '*','gfShopTheme')?></span>
                            <?php endif;?>
                            <input aria-label="Email" class="additionalInput" name="donationEmail" type="email" value="<?=$email?>" placeholder="<?php
                            _e('E-mail adresa', 'gfShopTheme') ?>"/>
                        </div>
                        <div class="col-12-sm show-div">
                            <!--					<input class="moneyInput" min="200" type="number" value="" placeholder="Unesi iznos" name="donationValue"/>-->
                            <!--					<span class="acceptedMinMoney">Minimalni iznos donacije je 200 RSD.</span>-->
                        </div>
                        <div class="col-4 col-12-sm">
                            <div class="switchBtns moneyInput">
                                <button id="oneTimeDonation" class="btn active"><?php
                                    _e('Jednokratno', 'gfShopTheme') ?></button>
                                <button id="recurringDonation" class="btn permActive <?=!$shouldSubBeActive ? 'disabledDonation': ''?>">
                                    <?php
                                    _e('Svakog meseca', 'gfShopTheme') ?> </button>
                                <input id="recurringPayment" type="hidden" name="recurringPayment" value="0">
                            </div>
                            <?php if(!$shouldSubBeActive):?>
                            <span class="<?=$noticeClass?>" id="loggedOutNoticeContainer"><?=$noticeMessage?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8 col-12-sm">
                            <label class="newsletter-checkbox">
                                <?php
                                _e('Doniraj anonimno', 'gfShopTheme'); ?>
                                <input type="checkbox" value="1" name="anonymousDonation">
                                <span class="checkmark"></span>
                            </label>
                            <?php
                            $defaultTokenId = get_user_meta(get_current_user_id(),'defaultTokenId',true);
                            if ($defaultTokenId !== '' && $defaultTokenId !== null && $defaultTokenId):?>
                            <label class="newsletter-checkbox">
                                <?=
                                __(sprintf('Koristi sačuvanu karticu %s (opciono)', $last4), 'gfShopTheme'); ?>
                                <input type="checkbox" value="1" name="payWithSavedCard">
                                <span class="checkmark"></span>
                            </label>
                            <?php endif;?>
                        </div>
                        <div class="col-4 col-12-sm">
                            <div class="donationBtn">
                                <input type="submit" value="<?php
                                _e('Doniraj', 'gfShopTheme') ?>"/>
                                <i class="fas fa-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="permanentAccount hide">
                    <div class="donationFormPermanentAccount">
                        <div class="row">
                            <div class="col-12">
                                <div class="PermanentAccountHeader"><?php
                                    _e('Broj meseci', 'gfShopTheme') ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8 col-12-sm">
                                <select aria-label="number of months" form="donationForm" id="donationDuration" class="selection" name="numberOfMonths">
                                    <?php
                                    foreach ($numberOfMonths as $key => $value): ?>
                                        <option value="<?=$value?>"><?=__($key, 'gfShopTheme')?></option>
                                    <?php
                                    endforeach; ?>
                                </select>
                            </div>
                        </div>
            </div>
        </div>
    </div>
<?php
else: ?><?php
endif; ?>