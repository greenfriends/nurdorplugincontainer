jQuery(document).ready(function ($) {

    var subInterval;
    // Set dimensions for sub images
    let sliderWidth = $('.slider').width();
    let sliderHeightElem = $('.slider-img').children('img')[0];
    let sliderHeight = $(sliderHeightElem).height();
    $('.subImagesContainer .subImage').each(function(){
        $(this).width(sliderWidth);
        $(this).height(sliderHeight);
    })

    function slideClick(button) {
        startSlider();
        $(button).addClass("active").siblings(".num").removeClass("active");

        let pag = parseInt($(button).text());
        $('.slider-container .slider').find('.slider-img').each(function () {
            if ($(this).data('count') === pag) {
                //add active class to image
                $(this).addClass('active').siblings('.active').removeClass('active');

                //animate the images
                $(".slider-container .slider").stop(true,true).animate({
                    top: -$(".slider-container").height() * $(button).index()
                }, 500);

            }
        })
    }

    function cycle() {
        let active = $('.slider-container .pagination .counter .num.active').removeClass('active');
        if (active.next() && active.next().length) {
            active.next().addClass('active');
        }
        else {
            active.siblings(":first").addClass('active');
        }
        let activeNum = parseInt(active.text());
        $('.slider-container .slider').find('.slider-img').each(function (i) {
            if ($(this).data('count') == activeNum) {
                handleSubImagesForSlide($(this).next('.slider-img'))
                var activeImg = $(this).removeClass('active');
                if (activeImg.next() && activeImg.next().length) {
                    activeImg.next().addClass('active');
                    $(this).parent().stop(true,true).animate({
                        top: -$(".slider-container").height() * ($(this).index() + 1)
                    }, 500)
                }
                else { // Last slide
                    // activeImg.siblings(":first").addClass('active');
                    // $(this).parent().animate({
                    //     top: 0
                    // }, 500);
                    // Do it this way to trigger the sub menus
                    $('.counter .num').first().click();
                }
            }
        });
    }

    var autoSlider = null;


    let imgHeightInterval = setInterval(() =>{
        if($('.slider-img').outerHeight() > 50) {
            $('.slider-container').outerHeight($('.slider-img').outerHeight());
            clearInterval(imgHeightInterval);
        }
    },500);
    $(window).on('resize', function () {
        $('.slider-container').outerHeight($('.slider-img').outerHeight());
    })
    let speed;
    if ($('.slider').data('speed') === '') {
        speed = 3000; //default
    } else {
        speed = $('.slider').data('speed');
    }

    function startSlider() {
        if (!autoSlider) {
            autoSlider = setInterval(cycle, speed);
        }
    }

    function pauseSlider() {
        if (autoSlider) {
            clearInterval(autoSlider);
            autoSlider = null;
            startSlider();
        }
    }

    function handleSubImagesForSlide(container) {
        let subImagesContainer = container.find('.subImagesContainer');
        let subImages = container.find('.subImagesContainer').find('.subImage');
        if(subImagesContainer.length > 0) {
            clearInterval(autoSlider);
            let numIterator = 0;
            subInterval = setInterval(function(){
                animateSubImage(subImagesContainer,numIterator)
                if(numIterator === subImages.length-1) {
                    clearInterval(subInterval);
                    setTimeout(function(){
                        let nextActive = $('.counter .num.active').next()
                        if(nextActive.length === 0) {
                            nextActive = $('.counter .num').first();
                        }
                        nextActive.click();

                        subImagesContainer.stop(true,true).animate({
                            left: '100%'
                        }, 500);
                    },3000)
                }
                numIterator++;
            },speed);
        }
    }

    function animateSubImage(container,numIterator) {
        let offset = '-' + numIterator + '00%';
        container.stop(true,true).animate({
            left: offset
        }, 500);
    }

    function resetAllSubSlides() {
        $('.subImagesContainer').stop(true,true).animate({
            left: '100%'
        }, 0);
        clearInterval(subInterval)
    }

    $(".slider-container .pagination .counter .num").on("click", function () {
        resetAllSubSlides();
        pauseSlider(); //pause slider for 2s when pagination is clicked
        slideClick(this);
        handleSubImagesForSlide($('.slider-img.active'));
    });

    startSlider();
})