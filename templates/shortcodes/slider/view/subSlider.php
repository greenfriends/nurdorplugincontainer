<img class="subImage" src="<?=$subImage['url']?>" alt="<?=$subImage['title']?>">
<div class="slider-box">
    <div class="title"><?= $subImage['title'] ?></div>
    <div class="description"><?= __($subImage['text'], 'gfShopTheme') ?></div>
</div>
<?php if($subImage['buttonText'] !== ''):?>
    <div class="link">
        <a href="<?= $subImage['linkTo'] ?>"><?php _e($subImage['buttonText'], 'gfShopTheme') ?></a>
        <i class="fas fa-arrow-right"></i>
    </div>
<?php endif; ?>