<?php
$options = get_option('gf_slider_options')['sliders'][$data['name']];
$speed = $options['slide_speed'];
$imageWidth = $options['image_width'];
$imageHeight = $options['image_height'];
$images = $options['images'];
$slidesCount = count($images);
?>
<div class="row">
    <div class="slider-container">
        <div class="slider" data-speed="<?= $speed ?>">
            <?php foreach ($options['images'] as $count => $image):
                $hasSubImages = isset($image['sub-image']);
                $title = $image['title'] ?? '';
                $description = $image['text'] ?? '';
                $link = $image['linkTo'] ?? '';
                $buttonText = $image['buttonText'] ?? '';
                if (is_plugin_active('wpml-translation-management/plugin.php')) {
                    global $sitepress;
                    $currentLang = $sitepress->get_current_language();
                    $link = apply_filters('wpml_permalink', $link, $currentLang);
                }

                $active = '';
                if (($count + 1) === 1) {
                    $active = ' active';
                }
                ?>
                <div class="slider-img<?= $active ?>" data-count=<?= $count + 1 ?>>
                    <img src="<?= $image['url']?>" width="<?= $imageWidth ?>" height="<?= $imageHeight ?>" alt="Slider image"/>
                    <?php
                    if($hasSubImages) {
                        echo '<div class="subImagesContainer">';
                        foreach($image['sub-image'] as $subImage) {
                            echo '<div class="subImageItem">';
                            include(__DIR__ . '/subSlider.php');
                            echo '</div>';
                        }
                        echo '</div>';
                    }
                    ?>
                    <div class="slider-box">
                        <div class="title"><?= $title ?></div>
                        <div class="description"><?= __($description, 'gfShopTheme') ?></div>
                    </div>
                    <?php if($buttonText !== ''):?>
                    <div class="link">
                        <a href="<?= $link ?>"><?php _e($buttonText, 'gfShopTheme') ?></a>
                        <i class="fas fa-arrow-right"></i>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <?php if($slidesCount > 1):?>
        <div class="pagination">
            <div class="counter" id="customCounter">
                <?php $activeClass = ''; ?>
                <?php
                for ($x = 0; $x < $slidesCount; $x++) :
                    $pag = $x + 1;
                    if($x < 10) {
                        $pag = '0' . $pag;
                    }
                    if (($x + 1) === 1):
                        ?>
                        <div class="num active" data-num="<?=$pag?>"><?= $pag ?></div>
                    <?php
                    else : ?>
                        <div class="num"><?= $pag ?></div>
                    <?php
                    endif;
                endfor;
                ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>